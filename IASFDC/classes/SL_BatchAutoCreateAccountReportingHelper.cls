public class SL_BatchAutoCreateAccountReportingHelper {
    
    public Static String createQuery() {
        
        String query = 'Select Id, Name, RecordTypeId, Original_Effective_Date__c ';
        for(Line_Of_Bussiness__mdt objLOB : getLOBMetadataRecords().values()) 
        {
            if(objLOB.MasterLabel != 'Add-On')
                query += ',' + objLOB.Active_Group__c + ',' + objLOB.Ann_Day__c + ',' + objLOB.Anniversary_Date__c + ',' + objLOB.Ann_Month__c ;
            if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' ||  objLOB.MasterLabel == 'Ancillary' || objLOB.MasterLabel == 'Vision' || objLOB.MasterLabel == 'Senior') {
                query += ',' + objLOB.Members__c + ',' + objLOB.Subscribers__c;
            }
            if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Senior') {
                query += ',' +  objLOB.Enrolled_Count__c + ',' + objLOB.Eligibles__c;
            }
        }
        
        return query;  
    }
    
    public static void autoCreateAccountReportingForAccount (List<SObject> scope) {
        
        List<Account_Reporting__c> lstToInsertAccReporting = new List<Account_Reporting__c>();
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = getLOBMetadataRecords();
        Map<Id,List<Add_On_Plan__c>> mapAccIdToLstOfAddOnPlan = new Map<Id,List<Add_On_Plan__c>>();
        set<Id> AccIds = new set<Id>();
        for(sObject objAccount : scope) {
            AccIds.add(objAccount.Id);
        }
        for(Add_On_Plan__c objAddOnPlan : [SELECT Id, Name, Account__c, Account_Name__c, Add_On_Name__c, Status__c FROM Add_On_Plan__c WHERE Status__c = 'Active' AND Account__c IN : AccIds]) {
             if (mapAccIdToLstOfAddOnPlan.containsKey(objAddOnPlan.Account__c)) {
                mapAccIdToLstOfAddOnPlan.get(objAddOnPlan.Account__c).add(objAddOnPlan);
            }
            else {
                mapAccIdToLstOfAddOnPlan.put(objAddOnPlan.Account__c, new List<Add_On_Plan__c>{ objAddOnPlan });
            }
        }
        
        for(SObject objSObj : scope) { 
            For(String strLOB : mapNameToLOB.keySet()) {
                Line_Of_Bussiness__mdt objLOB =mapNameToLOB.get(strLOB) ;
                Account_Reporting__c objAccRep = new Account_Reporting__c();
                objAccRep.Account__c = objSObj.Id;
                objAccRep.External_Id__c = objSObj.Id + '-' + strLOB;
                objAccRep.Name = (String.valueOf(objSObj.get('Name')).length() < 68 ? String.valueOf(objSObj.get('Name')) : String.valueOf(objSObj.get('Name')).substring(0,68)) + '-' + strLOB;
                objAccRep.Line_of_Business__c = strLOB;
                if(strLOB != 'Add-On') {
                    objAccRep.Ann_Day__c = Integer.valueOf(objSObj.get(objLOB.Ann_Day__c));
                    objAccRep.Anniversary_Date__c = String.valueOf(objSObj.get(objLOB.Anniversary_Date__c));
                    objAccRep.Ann_Month__c = Integer.valueOf(objSObj.get(objLOB.Ann_Month__c));
                    
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' ||  objLOB.MasterLabel == 'Ancillary' || objLOB.MasterLabel == 'Vision' || objLOB.MasterLabel == 'Senior') {
                        objAccRep.Members__c = Integer.valueOf(objSObj.get(objLOB.Members__c));
                        objAccRep.Subscribers__c = Integer.valueOf(objSObj.get(objLOB.Subscribers__c));
                    }
                    else {
                        objAccRep.Members__c = 0;
                        objAccRep.Subscribers__c = 0;
                    }
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Senior') {
                        //% SCS 2019-10-22 Fixed Net Eligibles and Enrolled Count fields
                    //  objAccRep.Enrolled_Count__c = Integer.valueOf(objSObj.get(objLOB.Eligibles__c));
                    //  objAccRep.Net_Eligibles__c = Integer.valueOf(objSObj.get(objLOB.Enrolled_Count__c));
                        objAccRep.Enrolled_Count__c = Integer.valueOf(objSObj.get(objLOB.Enrolled_Count__c));
                        objAccRep.Net_Eligibles__c = Integer.valueOf(objSObj.get(objLOB.Eligibles__c));
                    } else {
                        objAccRep.Enrolled_Count__c = 0;
                        objAccRep.Net_Eligibles__c =0;
                    }
                    if(Integer.valueOf(objSObj.get(objLOB.Active_Group__c)) > 0)
                        objAccRep.Account_Type__c = 'Customer'; 
                    else if(Integer.valueOf(objSObj.get(objLOB.Active_Group__c)) == 0 && (Integer.valueOf(objSObj.get('Active_Medical_Groups__c')) == 0
                                                                                          && Integer.valueOf(objSObj.get('Active_Dental_Groups__c')) == 0
                                                                                          && Integer.valueOf(objSObj.get('Active_Senior_Groups__c')) == 0)) 
                        objAccRep.Account_Type__c = 'Prospect';
                    else if(Integer.valueOf(objSObj.get(objLOB.Active_Group__c)) == 0 && (Integer.valueOf(objSObj.get('Active_Medical_Groups__c')) > 0
                                                                                          || Integer.valueOf(objSObj.get('Active_Dental_Groups__c')) > 0
                                                                                          || Integer.valueOf(objSObj.get('Active_Senior_Groups__c')) > 0))
                        objAccRep.Account_Type__c = 'Cross Sell';
                    
                 lstToInsertAccReporting.add(objAccRep);
                } else {
                    
                    if(mapAccIdToLstOfAddOnPlan.containsKey(objSObj.Id) && mapAccIdToLstOfAddOnPlan.get(objSObj.Id).size()>0)
                        objAccRep.Account_Type__c = 'Customer'; 
                    if(!mapAccIdToLstOfAddOnPlan.containsKey(objSObj.Id) && (Integer.valueOf(objSObj.get('Active_Medical_Groups__c')) == 0
                                                                                                                                  && Integer.valueOf(objSObj.get('Active_Dental_Groups__c')) == 0
                                                                                                                                  && Integer.valueOf(objSObj.get('Active_Senior_Groups__c')) == 0)) 
                        objAccRep.Account_Type__c = 'Prospect';
                    if(!mapAccIdToLstOfAddOnPlan.containsKey(objSObj.Id) && (Integer.valueOf(objSObj.get('Active_Medical_Groups__c')) > 0
                                                                                                                                  || Integer.valueOf(objSObj.get('Active_Dental_Groups__c')) > 0
                                                                                                                                  || Integer.valueOf(objSObj.get('Active_Senior_Groups__c')) > 0))
                        objAccRep.Account_Type__c = 'Cross Sell';
                    
                    lstToInsertAccReporting.add(objAccRep);
                }
            }
        }
        
        if(!lstToInsertAccReporting.isEmpty())
            upsert lstToInsertAccReporting External_Id__c;
    }
    
    public Static Map<String,Line_Of_Bussiness__mdt> getLOBMetadataRecords() {
        List<String> lstOfLOB = new List<String>();
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = new Map<String,Line_Of_Bussiness__mdt>();
        Schema.DescribeFieldResult fieldResult = Account_Reporting__c.Line_of_Business__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            lstOfLOB.add(pickListVal.getLabel());
        }   
        
        for(Line_Of_Bussiness__mdt objLOB :  [Select Id, DeveloperName, MasterLabel, Active_Group__c, Ann_Day__c, Anniversary_Date__c, Ann_Month__c, Eligibles__c, Enrolled_Count__c, Members__c, Subscribers__c
                                              From Line_Of_Bussiness__mdt
                                              Where MasterLabel IN : lstOfLOB]) {
                                                  
                                                  mapNameToLOB.put(objLOB.MasterLabel, objLOB);      
                                              }
        
        return mapNameToLOB;
    }
    
    public static List<RecordType> getRecordType() {
        
        List<String> lstRecTypeName = new List<String>{'Customer', 'Prospect','Small Group Customer','Small Group Prospect','JPA Account','JPA Child Account','JPA Child Prospect Account'};
            
        return [Select Id From RecordType Where SobjectType = 'Account' AND Name IN : lstRecTypeName];
    }   
}