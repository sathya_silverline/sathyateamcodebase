public class SL_BatchAutoCreateOppReportingHelper {
    
    public static void autoCreateOpportunityReporting(List<sObject> scope, List<Integer> lstYears) {
        
        Map<Id, Opportunity> mapAccRepIdToFinalOpp = new Map<Id, Opportunity>();
        Map<Id, List<Account_reporting__c>> mapAccIdToLstAccountReporting = new Map<Id, List<Account_reporting__c>>();
        Map<Id, Account_reporting__c> mapAccRepIdToAccountRep = new Map<Id, Account_reporting__c>();
        Map<Id, Account> mapAccIdToAccount = new Map<Id, Account>();
       	Map<String, String> mapOppStage = getStageValue();
        
        for(sObject objAcc : scope) {
            mapAccIdToAccount.put(Id.valueof(objAcc.Id),(Account)objAcc);
        }
        
        for(Account_reporting__c objAccRep : [Select Id, Name, Account__c,  Account_Type__c, Line_of_Business__c From Account_reporting__c Where Account__c IN : mapAccIdToAccount.Keyset()] ) {
            mapAccRepIdToAccountRep.put(objAccRep.Id, objAccRep);
            if(mapAccIdToLstAccountReporting.containsKey(objAccRep.Account__c))
                mapAccIdToLstAccountReporting.get(objAccRep.Account__c).add(objAccRep);
            else
                mapAccIdToLstAccountReporting.put(objAccRep.Account__c, new List<Account_reporting__c>{objAccRep});
        }
        
        List<Opportunity> lstTempOpportunity = [Select Id, Name, AccountId, Effective_Date__c, StageName, RecordType.Name, Net_Eligibles__c,
                                            Net_Dental_Eligibles__c, Net_Medical_Eligibles__c, Net_Senior_Eligibles__c, CloseDate,
                                            (Select Id, Name, Synced__c, Opportunity_Account_Name__c, OpportunityId
                                             FROM Quotes Where Synced__c = True)
                                            FROM Opportunity 
                                            Where  AccountId IN : mapAccIdToLstAccountReporting.keyset() 
                                            ORDER BY Effective_Date__c, CloseDate];
        
        Map<Id, List<Opportunity>> mapAccIdToLstOfOpp = new Map<Id,List<Opportunity>>();
        Map<Integer, List<Opportunity>> mapYearToLstOfOpp = new Map<Integer,List<Opportunity>>();
        Set<Id> quoteIds = new Set<Id>();
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        for(Opportunity objOpp : lstTempOpportunity) {
            if(objOpp.Effective_Date__c != null) {
                if(lstYears.contains(objOpp.Effective_Date__c.Year())){
            	lstOpportunity.add(objOpp);
            
                    for(Quote objQuote : objOpp.Quotes) {
                        quoteIds.add(objQuote.Id);
                    }
                    
                    if(mapAccIdToLstOfOpp.containsKey(objOpp.AccountId))
                        mapAccIdToLstOfOpp.get(objOpp.AccountId).add(objOpp);
                    else
                        mapAccIdToLstOfOpp.put(objOpp.AccountId, new List<Opportunity>{objOpp});
                    
                }
            } else if(objOpp.CloseDate != null) {
                if(lstYears.contains(objOpp.CloseDate.Year())){ 
                    lstOpportunity.add(objOpp);
                	for(Quote objQuote : objOpp.Quotes) {
                        quoteIds.add(objQuote.Id);
                    }
                    
                    if(mapAccIdToLstOfOpp.containsKey(objOpp.AccountId))
                        mapAccIdToLstOfOpp.get(objOpp.AccountId).add(objOpp);
                    else
                        mapAccIdToLstOfOpp.put(objOpp.AccountId, new List<Opportunity>{objOpp});
                }
            }
            
        }
        
        
        List<WrapperForOppReporting> lstWrapper = new List<WrapperForOppReporting>();
        Map<Id, List<Opportunity>> mapAccRepTolstOpp = new Map<Id,List<Opportunity>>();
        Map<Id, List<Quote_Plan__c>> mapOppIdToSoldQuotePlans = new Map<Id,List<Quote_Plan__c>>();
        Map<Id, List<Quote_Plan__c>> mapOppIdToQuotingQuotePlans = new Map<Id,List<Quote_Plan__c>>();
        Map<Id, List<Quote_Plan__c>> mapOppIdToCurrentQuotePlans = new Map<Id,List<Quote_Plan__c>>();
        Map<Id, List<Quote_Plan__c>> mapOppIdToIBRQuotePlans = new Map<Id,List<Quote_Plan__c>>();
        
        // Creating a Map of Opportunity Id to quote plane
        For(Quote_Plan__c objQuotePlan : [Select Id, Name, Quote__c, Quote_Plan_Type__c, Quote__r.Id, Quote__r.Name, Quote__r.Synced__c, Quote__r.OpportunityId, Quote__r.Opportunity.RecordType.Name,Quote__r.Opportunity.AccountId, Product_Line__c From Quote_Plan__c Where Quote__c IN : quoteIds AND Quote__r.Synced__c = true] ) {
           
            if(objQuotePlan.Quote_Plan_Type__c == 'Sold') {
                if(mapOppIdToSoldQuotePlans.containsKey(objQuotePlan.Quote__r.OpportunityId))
                    mapOppIdToSoldQuotePlans.get(objQuotePlan.Quote__r.OpportunityId).add(objQuotePlan);
                else
                    mapOppIdToSoldQuotePlans.put(objQuotePlan.Quote__r.OpportunityId, new List<Quote_Plan__c>{objQuotePlan});
            }
            if(objQuotePlan.Quote_Plan_Type__c == 'Quoting') {
                if(mapOppIdToQuotingQuotePlans.containsKey(objQuotePlan.Quote__r.OpportunityId))
                    mapOppIdToQuotingQuotePlans.get(objQuotePlan.Quote__r.OpportunityId).add(objQuotePlan);
                else
                    mapOppIdToQuotingQuotePlans.put(objQuotePlan.Quote__r.OpportunityId, new List<Quote_Plan__c>{objQuotePlan});
            }
            if(objQuotePlan.Quote_Plan_Type__c == 'Current') {
                if(mapOppIdToCurrentQuotePlans.containsKey(objQuotePlan.Quote__r.OpportunityId))
                    mapOppIdToCurrentQuotePlans.get(objQuotePlan.Quote__r.OpportunityId).add(objQuotePlan);
                else
                    mapOppIdToCurrentQuotePlans.put(objQuotePlan.Quote__r.OpportunityId, new List<Quote_Plan__c>{objQuotePlan});
            }
            if(objQuotePlan.Quote_Plan_Type__c == 'Pre-IBR') {
                if(mapOppIdToIBRQuotePlans.containsKey(objQuotePlan.Quote__r.OpportunityId))
                    mapOppIdToIBRQuotePlans.get(objQuotePlan.Quote__r.OpportunityId).add(objQuotePlan);
                else
                    mapOppIdToIBRQuotePlans.put(objQuotePlan.Quote__r.OpportunityId, new List<Quote_Plan__c>{objQuotePlan});
            }
        }
        
        for(Opportunity objOpp : lstOpportunity) {
                for(Account_reporting__c objAccRep : mapAccIdToLstAccountReporting.get(objOpp.AccountId)) { 
                    //checking if the recordtype of the opportunity is Small group and iterating over it
                    if(objOpp.RecordType.Name.contains('Small Group')) {
                        if(mapOppIdToSoldQuotePlans.containsKey(objOpp.Id) && !mapOppIdToSoldQuotePlans.get(objOpp.Id).isEmpty()) {
                            for(Quote_Plan__c objQuotePlan : mapOppIdToSoldQuotePlans.get(objOpp.Id)) {
                                if(objQuotePlan.Product_Line__c == objAccRep.Line_of_Business__c) {
                                    if(mapAccRepTolstOpp.containsKey(objAccRep.Id))
                                        mapAccRepTolstOpp.get(objAccRep.Id).add(objOpp);
                                    else
                                        mapAccRepTolstOpp.put(objAccRep.Id, new List<Opportunity>{objOpp});
                                }
                            }
                        }
                        else if(mapOppIdToQuotingQuotePlans.containsKey(objOpp.Id) && !mapOppIdToQuotingQuotePlans.get(objOpp.Id).isEmpty()) {
                            for(Quote_Plan__c objQuotePlan : mapOppIdToQuotingQuotePlans.get(objOpp.Id)) {
                                if(objQuotePlan.Product_Line__c == objAccRep.Line_of_Business__c) {
                                    if(mapAccRepTolstOpp.containsKey(objAccRep.Id))
                                        mapAccRepTolstOpp.get(objAccRep.Id).add(objOpp);
                                    else
                                        mapAccRepTolstOpp.put(objAccRep.Id, new List<Opportunity>{objOpp});
                                }
                            }
                        }
                        else if(mapOppIdToCurrentQuotePlans.containsKey(objOpp.Id) && !mapOppIdToCurrentQuotePlans.get(objOpp.Id).isEmpty()) {
                            for(Quote_Plan__c objQuotePlan : mapOppIdToCurrentQuotePlans.get(objOpp.Id)) {
                                if(objQuotePlan.Product_Line__c == objAccRep.Line_of_Business__c) {
                                    if(mapAccRepTolstOpp.containsKey(objAccRep.Id))
                                        mapAccRepTolstOpp.get(objAccRep.Id).add(objOpp);
                                    else
                                        mapAccRepTolstOpp.put(objAccRep.Id, new List<Opportunity>{objOpp});
                                }
                            }
                        }
                        else if(mapOppIdToIBRQuotePlans.containsKey(objOpp.Id) && !mapOppIdToIBRQuotePlans.get(objOpp.Id).isEmpty()) {
                            for(Quote_Plan__c objQuotePlan : mapOppIdToIBRQuotePlans.get(objOpp.Id)) {
                                if(objQuotePlan.Product_Line__c == objAccRep.Line_of_Business__c) {
                                    if(mapAccRepTolstOpp.containsKey(objAccRep.Id))
                                        mapAccRepTolstOpp.get(objAccRep.Id).add(objOpp);
                                    else
                                        mapAccRepTolstOpp.put(objAccRep.Id, new List<Opportunity>{objOpp});
                                }
                            }
                        }
                    } // Iterating over the large group
                    else if(objAccRep.Line_of_Business__c != null && objOpp.Net_Eligibles__c > 50 && objOpp.RecordType.Name.contains(objAccRep.Line_of_Business__c)){
                        if(mapAccRepTolstOpp.containsKey(objAccRep.Id))
                            mapAccRepTolstOpp.get(objAccRep.Id).add(objOpp);
                        else
                            mapAccRepTolstOpp.put(objAccRep.Id, new List<Opportunity>{objOpp});
                    }
                }
            }
        //}
        
        
        Map<String, Integer> stages = new Map<String, Integer>();
        Integer index = 0;
        for(PicklistEntry value: Opportunity.StageName.getDescribe().getPicklistValues()) {
          stages.put(value.getValue(), index++);
        }
        
        for(Account objAccount : mapAccIdToAccount.values()){
            for(Account_Reporting__c objAccRep :  mapAccIdToLstAccountReporting.get(objAccount.Id)){
                if(mapAccRepTolstOpp.containskey(objAccRep.Id)) {
                    Map<Integer, List<Opportunity>> mapYearToclosedWonOpps = new Map<Integer, List<Opportunity>>();
                    Map<Integer, List<Opportunity>> mapYearToOpenOpps = new Map<Integer, List<Opportunity>>();
                    Map<Integer, List<Opportunity>> mapYearToClosedLostOpps = new Map<Integer, List<Opportunity>>();
                    for(Opportunity objOpp : mapAccRepTolstOpp.get(objAccRep.Id)) {
                        if(objOpp.Effective_Date__c != null) {
                            system.debug('>Inside Eff>');
                            if(mapOppStage.get(objOpp.StageName) == 'Closed/Won') {
                                if(mapYearToclosedWonOpps.containsKey(objOpp.Effective_Date__c.Year()))
                                    mapYearToclosedWonOpps.get(objOpp.Effective_Date__c.Year()).add(objOpp);
                                else
                                    mapYearToclosedWonOpps.put(objOpp.Effective_Date__c.Year(), new List<Opportunity>{objOpp});
                            }
                            else if(mapOppStage.get(objOpp.StageName) == 'Open') {
                                if(mapYearToOpenOpps.containsKey(objOpp.Effective_Date__c.Year()))
                                    mapYearToOpenOpps.get(objOpp.Effective_Date__c.Year()).add(objOpp);
                                else
                                    mapYearToOpenOpps.put(objOpp.Effective_Date__c.Year(), new List<Opportunity>{objOpp});
                            }
                            else if(mapOppStage.get(objOpp.StageName) == 'Closed/Lost') {
                                if(mapYearToClosedLostOpps.containsKey(objOpp.Effective_Date__c.Year()))
                                    mapYearToClosedLostOpps.get(objOpp.Effective_Date__c.Year()).add(objOpp);
                                else
                                    mapYearToClosedLostOpps.put(objOpp.Effective_Date__c.Year(), new List<Opportunity>{objOpp});
                            }
                        } else if(objOpp.CloseDate != null) {
                            system.debug('>Inside close>');
                            if(mapOppStage.get(objOpp.StageName) == 'Closed/Won') {
                                if(mapYearToclosedWonOpps.containsKey(objOpp.CloseDate.Year()))
                                    mapYearToclosedWonOpps.get(objOpp.CloseDate.Year()).add(objOpp);
                                else
                                    mapYearToclosedWonOpps.put(objOpp.CloseDate.Year(), new List<Opportunity>{objOpp});
                            }
                            else if(mapOppStage.get(objOpp.StageName) == 'Open') {
                                if(mapYearToOpenOpps.containsKey(objOpp.CloseDate.Year()))
                                    mapYearToOpenOpps.get(objOpp.CloseDate.Year()).add(objOpp);
                                else
                                    mapYearToOpenOpps.put(objOpp.CloseDate.Year(), new List<Opportunity>{objOpp});
                            }
                            else if(mapOppStage.get(objOpp.StageName) == 'Closed/Lost') {
                                if(mapYearToClosedLostOpps.containsKey(objOpp.CloseDate.Year()))
                                    mapYearToClosedLostOpps.get(objOpp.CloseDate.Year()).add(objOpp);
                                else
                                    mapYearToClosedLostOpps.put(objOpp.CloseDate.Year(), new List<Opportunity>{objOpp});
                            }
                        }
                        
                    }
                    
                    for(Integer year : lstYears) {
                        Opportunity objOpp ;
                        if(mapYearToclosedWonOpps.get(year) != null && !mapYearToclosedWonOpps.get(year).isEmpty() && mapYearToclosedWonOpps.get(year).size()>=1) 
                           objOpp = getLatestOpportunity(mapYearToclosedWonOpps.get(year),year);
                        else if (mapYearToOpenOpps.get(year) != null &&  !mapYearToOpenOpps.get(year).isEmpty() && mapYearToOpenOpps.get(year).size()>=1) 
                            	objOpp = getLatestOpportunity(mapYearToOpenOpps.get(year),year);
                        else if (mapYearToClosedLostOpps.get(year) != null && !mapYearToClosedLostOpps.get(year).isEmpty() && mapYearToClosedLostOpps.get(year).size()>=1) 
                            	objOpp = getLatestOpportunity(mapYearToClosedLostOpps.get(year),year);
                        
                        if(objOpp != null)
                            lstWrapper.add( new WrapperForOppReporting(mapAccIdToAccount.get(mapAccRepIdToAccountRep.get(objAccRep.Id).Account__c), mapAccRepIdToAccountRep.get(objAccRep.Id), objOpp, year));
                        
                    } 
                    
                }
                else{
                        for(Integer year : lstYears) { 
                        	lstWrapper.add( new WrapperForOppReporting(mapAccIdToAccount.get(mapAccRepIdToAccountRep.get(objAccRep.Id).Account__c), mapAccRepIdToAccountRep.get(objAccRep.Id), null, year )); 
                        }
                }
            }
        }
        system.debug('>>>.'+lstWrapper);
        createOpportunityReporting(lstWrapper,mapOppStage);
    }
        
    
    // Method to get the latest Opportunity based on Effective date
    public static Opportunity getLatestOpportunity(List<Opportunity> lstOpp, Integer Year){
        Date latestDateCurrent = date.newinstance(Year, 1, 1);
        Opportunity objOpp1;
        for(Opportunity objOpp : lstOpp) {
            if(objOpp.Effective_Date__c != null) {
               if(objOpp.Effective_Date__c >=latestDateCurrent) {
                    objOpp1 = objOpp;
                    latestDateCurrent = objOpp.Effective_Date__c;
                } 
            } else if(objOpp.CloseDate != null) {
                	objOpp1 = objOpp;
                    latestDateCurrent = objOpp.CloseDate;
            }
                
        }
        
        return objOpp1;
    }
    
    public static Map<String, String> getStageValue() {
        Map<String, String> mapKeyVal = new Map<String, String>();
        for(OpportunityStage os : [Select ApiName, IsClosed, IsWon FROM OpportunityStage WHERE ISActive = true]) {
            if(!os.IsClosed){
                mapKeyVal.put(os.ApiName, 'Open');
            }else if(os.IsClosed && os.IsWon) {
                mapKeyVal.put(os.ApiName, 'Closed/Won');
            }else {
                mapKeyVal.put(os.ApiName, 'Closed/Lost');
            }
        }
        return mapKeyVal;
    }
    
    Public Static void createOpportunityReporting(List<WrapperForOppReporting> lstWrapper, Map<String,String> mapOppStage) {
        List<Opportunity_Reporting__c> lstOppRep = new List<Opportunity_Reporting__c>();
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = SL_BatchAutoCreateAccountReportingHelper.getLOBMetadataRecords();
        for(WrapperForOppReporting objWrapperForOppReporting : lstWrapper ) {
            if(objWrapperForOppReporting.objOpportunity != null ) {
                    Opportunity_Reporting__c objOppRep = new Opportunity_Reporting__c();
                    objOppRep.External_Id__c = objWrapperForOppReporting.objAccount.Id + objWrapperForOppReporting.objAccReporting.Line_of_Business__c + objWrapperForOppReporting.Year;
                    objOppRep.Name = (objWrapperForOppReporting.objOpportunity.Name.length() < 68 ? objWrapperForOppReporting.objOpportunity.Name : objWrapperForOppReporting.objOpportunity.Name.substring(0,68))+'-'+ objWrapperForOppReporting.Year;
                    objOppRep.Account__c = objWrapperForOppReporting.objAccount.Id;
                    objOppRep.Effective_Date__c = objWrapperForOppReporting.objOpportunity.Effective_Date__c != null ? objWrapperForOppReporting.objOpportunity.Effective_Date__c : Date.newInstance(objWrapperForOppReporting.Year, 1, 1);
                    objOppRep.Line_of_Business__c = objWrapperForOppReporting.objAccReporting.Line_of_Business__c;
                    objOppRep.Opportunity__c = objWrapperForOppReporting.objOpportunity.Id;
                    
                    if(mapOppStage.get(objWrapperForOppReporting.objOpportunity.StageName) == 'Closed/Won')
                    	objOppRep.Sale_Outcome__c = 'Sold';
                    if(mapOppStage.get(objWrapperForOppReporting.objOpportunity.StageName) == 'Open')
                        objOppRep.Sale_Outcome__c = 'Open';
                    if(mapOppStage.get(objWrapperForOppReporting.objOpportunity.StageName) == 'Closed/Lost')
                        objOppRep.Sale_Outcome__c = 'Lost';
                    
                    if(objWrapperForOppReporting.objOpportunity.RecordType.Name == 'Small Group New Business' || (objWrapperForOppReporting.objOpportunity.RecordType.Name.contains('Renewal') && objWrapperForOppReporting.objAccReporting.Account_Type__c == 'Prospect')) 
                       objOppRep.Sale_Type__c = 'Prospects'; 
                    else if((objWrapperForOppReporting.objOpportunity.RecordType.Name == 'Small Group Renewal' && objWrapperForOppReporting.objAccReporting.Account_Type__c == 'Customer') || objWrapperForOppReporting.objOpportunity.RecordType.Name.contains('Renewal') ) 
                       objOppRep.Sale_Type__c = 'Renewal'; 
                    else if(objWrapperForOppReporting.objAccReporting.Account_Type__c == 'Cross Sell')
                       objOppRep.Sale_Type__c = 'Cross sell';  
                    
                    if(objOppRep.Line_of_Business__c == 'Medical' || objOppRep.Line_of_Business__c == 'Dental' || objOppRep.Line_of_Business__c=='Senior')
                        objOppRep.Net_Eligibles__c = Integer.valueOf(objWrapperForOppReporting.objOpportunity.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Eligibles__c));
                    
                    if(objOppRep.Line_of_Business__c != 'Add-On') {
                        objOppRep.Ann_Day__c = Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Day__c));
                        objOppRep.Anniversary_Date__c = String.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Anniversary_Date__c));
                        objOppRep.Ann_Month__c = Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Month__c));
                    }
                    
                    lstOppRep.add(objOppRep);
                
            } else {
                	Opportunity_Reporting__c objOppRep = new Opportunity_Reporting__c();
                	objOppRep.External_Id__c = objWrapperForOppReporting.objAccount.Id + objWrapperForOppReporting.objAccReporting.Line_of_Business__c + objWrapperForOppReporting.Year;
                	objOppRep.Name = (objWrapperForOppReporting.objAccount.Name.length() < 68 ? objWrapperForOppReporting.objAccount.Name : objWrapperForOppReporting.objAccount.Name.substring(0,68))+'-'+ objWrapperForOppReporting.Year;
                    objOppRep.Account__c = objWrapperForOppReporting.objAccount.Id;
                	objOppRep.Line_of_Business__c = objWrapperForOppReporting.objAccReporting.Line_of_Business__c;
                	objOppRep.Opportunity__c = null;
                	objOppRep.Sale_Outcome__c = 'Not Quoted';
                	objOppRep.Net_Eligibles__c = 0;
                	objOppRep.Effective_Date__c = Date.newInstance(objWrapperForOppReporting.Year, 1, 1);
                	
                    if(objOppRep.Line_of_Business__c == null) continue;
                	if(objOppRep.Line_of_Business__c != 'Add-On') {
                        objOppRep.Ann_Day__c = Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Day__c)) != null ? Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Day__c)) : null;
                        objOppRep.Anniversary_Date__c = String.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Anniversary_Date__c)) != null ? String.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Anniversary_Date__c)) : '';
                        objOppRep.Ann_Month__c = Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Month__c)) != null ? Integer.valueOf(objWrapperForOppReporting.objAccount.get(mapNameToLOB.get(objOppRep.Line_of_Business__c).Ann_Month__c)) : null;
                    }
                
                lstOppRep.add(objOppRep);
                
            }
        }
        //Insert Opportunity Reporting.
        if(!lstOppRep.isEmpty())
            upsert lstOppRep External_Id__c;
    }
    
    Public Class WrapperForOppReporting{
        Account objAccount;
        Account_reporting__c objAccReporting;
        Opportunity objOpportunity;
        Integer Year;
        
        public WrapperForOppReporting(Account objAccount, Account_reporting__c objAccReporting, Opportunity objOpportunity, Integer Year) {
           this.objAccount = objAccount;
           this.objAccReporting = objAccReporting;  
            this.objOpportunity = objOpportunity;
            this.Year = Year;
        }
        
    }
}