global class SL_BatchAutoCreateAccRepSchedular implements schedulable {
	global void execute(SchedulableContext sc)
    {
      SL_BatchAutoCreateAccountReporting b = new SL_BatchAutoCreateAccountReporting(); 
      database.executebatch(b);
    }
}