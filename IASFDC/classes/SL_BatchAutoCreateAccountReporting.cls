global class SL_BatchAutoCreateAccountReporting implements Database.Batchable<SObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Set<Id> recTypeIds = new Set<Id>();
        for(RecordType objRecType : SL_BatchAutoCreateAccountReportingHelper.getRecordType()) {
            recTypeIds.add(objRecType.Id);
        }
        
        String query = SL_BatchAutoCreateAccountReportingHelper.createQuery();
        
        query += ' From Account ';
        query += 'WHERE RecordTypeId IN : recTypeIds' ; 
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {	
        SL_BatchAutoCreateAccountReportingHelper.autoCreateAccountReportingForAccount(scope);
        
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
}