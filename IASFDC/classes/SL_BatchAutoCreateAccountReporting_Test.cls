@isTest
public class SL_BatchAutoCreateAccountReporting_Test {
    
    @testSetup static void createData() {
        List<Account> lstAccount = new List<Account>();
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = SL_BatchAutoCreateAccountReportingHelper.getLOBMetadataRecords();
        List<RecordType> lstRecType = SL_BatchAutoCreateAccountReportingHelper.getRecordType();
        For(Integer i=0; i<200; i++) {
            Account objAccount = new Account();
            objAccount.Name = 'Test Account - ' + i;
            objAccount.RecordTypeId = lstRecType[math.mod(i,7)].Id;
            objAccount.BillingCity = 'Test City'+i;
            objAccount.Ancillary__c = true;
            objAccount.BillingState = 'AZ';
            objAccount.BillingPostalCode ='85001';
            objAccount.BillingStreet = 'Test Street';
            for(Line_Of_Bussiness__mdt objLOB : mapNameToLOB.values()) {
                if(objLOB.MasterLabel != 'Add-On') {
                    
                    objAccount.put(objLOB.Ann_Day__c, 15);
                    objAccount.put(objLOB.Ann_Month__c, 4);
                    objAccount.put(objLOB.Anniversary_Date__c, '15-Apr');
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Ancillary' || objLOB.MasterLabel == 'Vision' || objLOB.MasterLabel == 'Senior') {
                        objAccount.put(objLOB.Members__c, 15);
                    	objAccount.put(objLOB.Subscribers__c, 4);
                    }
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Senior') {
                        objAccount.put(objLOB.Eligibles__c, 15);
                    	objAccount.put(objLOB.Enrolled_Count__c, 4);
                    }
                }
            }
            lstAccount.add(objAccount);
        }
        insert lstAccount;
    }
    
    @istest
    static void autoCreateAccountReportingTest() {
        test.startTest();
            SL_BatchAutoCreateAccRepSchedular m = new SL_BatchAutoCreateAccRepSchedular();
            String sch = '0 0 23 * * ?';
            String jobID = system.schedule('Auto Create Account Reporting', sch, m);
            m.execute(null);
        test.stopTest();
        system.assertEquals(1400, [Select Id From Account_Reporting__c].size());
        m.execute(null);
    }
}