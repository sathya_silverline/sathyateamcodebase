global class SL_BatchAutoCreateOpportunityReporting implements Database.Batchable<SObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Set<Id> recTypeIds = new Set<Id>();
        for(RecordType objRecType : SL_BatchAutoCreateAccountReportingHelper.getRecordType()) {
            recTypeIds.add(objRecType.Id);
        }
        String query = SL_BatchAutoCreateAccountReportingHelper.createQuery();
        query += ' From Account ';
        query += 'WHERE RecordTypeId IN : recTypeIds';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {	
        
        Years_for_Opportunity_Reporting__c objYearSetting = [Select Id, Name, Year__c from Years_for_Opportunity_Reporting__c Where Name = 'Default' Limit 1];
        List<String> lstStrYears = objYearSetting.Year__c.split(',');
        List<Integer> lstYears = new List<Integer>();
        Integer CurrentYear = system.today().year();
        for(String str : lstStrYears) {
            
            switch on str.trim() {
                when 'This Year'  {lstYears.add(CurrentYear); }
                when 'Last Year'  {lstYears.add(CurrentYear-1); }
                when 'Next Year'  {lstYears.add(CurrentYear+1); }
                when 'Two Year Ago' {lstYears.add(CurrentYear-2);}
            }
        }
        SL_BatchAutoCreateOppReportingHelper.autoCreateOpportunityReporting(scope, lstYears);
        
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
}