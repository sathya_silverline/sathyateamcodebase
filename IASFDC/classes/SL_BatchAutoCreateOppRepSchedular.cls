global class SL_BatchAutoCreateOppRepSchedular implements schedulable {
	global void execute(SchedulableContext sc)
    {
      SL_BatchAutoCreateOpportunityReporting b = new SL_BatchAutoCreateOpportunityReporting(); 
      database.executebatch(b,200);
    }
}