public class SL_BatchCreateAccRepSubsAndMemberHelper {
	
    public static void autoCreateAccountReportingSubscriberAndMember(List<SObject> scope) {
        Map<Id, Account> mapAccIdToAccount = new Map<Id, Account>();
        Map<Id, Account_reporting__c> mapAccRepIdToAccountRep = new Map<Id, Account_reporting__c>();
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = SL_BatchAutoCreateAccountReportingHelper.getLOBMetadataRecords();
        List<String> lstLOB = new List<String>{'Medical','Dental','Senior','Ancillary','Vision'};
        
        for(sObject objAcc : scope) {
            mapAccIdToAccount.put(Id.valueof(objAcc.Id),(Account)objAcc);
        }
        
        Map<Id, List<Account_reporting__c>> mapAccIdToLstAccountReporting = new Map<Id, List<Account_reporting__c>>();
        for(Account_reporting__c objAccRep : [Select Id, Name, Account__c,  Account_Type__c, Line_of_Business__c From Account_reporting__c Where Account__c IN : mapAccIdToAccount.Keyset() AND Line_of_Business__c IN : lstLOB] ) {
            mapAccRepIdToAccountRep.put(objAccRep.Id, objAccRep);
            if(mapAccIdToLstAccountReporting.containsKey(objAccRep.Account__c))
                mapAccIdToLstAccountReporting.get(objAccRep.Account__c).add(objAccRep);
            else
                mapAccIdToLstAccountReporting.put(objAccRep.Account__c, new List<Account_reporting__c>{objAccRep});
        }
        
        YearsForAccReportingSubscriberAndMember__c objYearSetting = [Select Id, Name, Year__c from YearsForAccReportingSubscriberAndMember__c Where Name = 'Default' Limit 1];
        List<String> lstStrYears = objYearSetting.Year__c.split(',');
        List<Integer> lstYears = new List<Integer>();
        Integer CurrentYear = system.today().year();
        for(String str : lstStrYears) {
            
            switch on str.trim() {
                when 'This Year'  {lstYears.add(CurrentYear); }
                when 'Last Year'  {lstYears.add(CurrentYear-1); }
                when 'Two Years Ago'  {lstYears.add(CurrentYear-2); }
            }
        }
        
        List<Account_Reporting_Subscribers_Members__c> lstOfAccRepSubscribersAndMembersToUpsert = new List<Account_Reporting_Subscribers_Members__c>();
        Map<String, Fields_For_AccRepSubscriberMember__mdt> mapMonthToFields = new Map<String, Fields_For_AccRepSubscriberMember__mdt>();
        for(Fields_For_AccRepSubscriberMember__mdt objMDT : [Select Id, MasterLabel, Members__c, Subscriber__c From Fields_For_AccRepSubscriberMember__mdt]) {
            mapMonthToFields.put(objMDT.MasterLabel, objMDT);
        }
        For(Integer year : lstYears) {
            for(Account objAccount : mapAccIdToAccount.values()) {
                for(Account_reporting__c objAccRep : mapAccIdToLstAccountReporting.get(objAccount.Id)) {
                    Account_Reporting_Subscribers_Members__c objAccRepSubMem = new Account_Reporting_Subscribers_Members__c();
                    objAccRepSubMem.Name = objAccRep.Name.length()>75 ? objAccRep.Name : objAccRep.Name.substring(0,75) +'-'+year;
                    objAccRepSubMem.Account_Reporting__c = objAccRep.Id;
                    objAccRepSubMem.External_Id__c = objAccRep.Id + '-' + objAccRep.Line_of_Business__c +'-'+year;
                    objAccRepSubMem.Year__c = String.valueOf(year);
                    objAccRepSubMem.put(mapMonthToFields.get(String.valueOf(system.today().month())).Subscriber__c, objAccount.get(mapNameToLOB.get(objAccRep.Line_of_Business__c).Subscribers__c)); 
                    objAccRepSubMem.put(mapMonthToFields.get(String.valueOf(system.today().month())).Members__c, objAccount.get(mapNameToLOB.get(objAccRep.Line_of_Business__c).Members__c)); 
                }
            }
        }
        
        
    }
}