@isTest
public class SL_BatchAutoCreateOppReporting_Test {
	
    @testSetup static void createData() {
        List<Account> lstAccount = new List<Account>();
        Years_for_Opportunity_Reporting__c objYearOpp = new Years_for_Opportunity_Reporting__c(Name = 'Default',
                                                                                               Year__c = 'This Year, Next Year');
        insert objYearOpp;
        
        Map<String,Line_Of_Bussiness__mdt> mapNameToLOB = SL_BatchAutoCreateAccountReportingHelper.getLOBMetadataRecords();
        List<RecordType> lstRecType = SL_BatchAutoCreateAccountReportingHelper.getRecordType();
        For(Integer i=0; i<3; i++) {
            Account objAccount = new Account();
            objAccount.Name = 'Test Account - ' + i;
            objAccount.RecordTypeId = lstRecType[math.mod(i,7)].Id;
            objAccount.BillingCity = 'Test City'+i;
            objAccount.Ancillary__c = true;
            objAccount.BillingState = 'AZ';
            objAccount.BillingPostalCode ='85001';
            objAccount.BillingStreet = 'Test Street';
             objAccount.Original_Effective_Date__c = Date.newInstance(system.today().Year(), 4, 5);
            for(Line_Of_Bussiness__mdt objLOB : mapNameToLOB.values()) {
                if(objLOB.MasterLabel != 'Add-On') {
                   
                    objAccount.put(objLOB.Ann_Day__c, 15);
                    objAccount.put(objLOB.Ann_Month__c, 4);
                    objAccount.put(objLOB.Anniversary_Date__c, '15-Apr');
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Ancillary' || objLOB.MasterLabel == 'Vision' || objLOB.MasterLabel == 'Senior') {
                        objAccount.put(objLOB.Members__c, 15);
                    	objAccount.put(objLOB.Subscribers__c, 4);
                    }
                    if(objLOB.MasterLabel == 'Medical' || objLOB.MasterLabel == 'Dental' || objLOB.MasterLabel == 'Senior') {
                        objAccount.put(objLOB.Eligibles__c, 15);
                    	objAccount.put(objLOB.Enrolled_Count__c, 4);
                    }
                }
            }
            lstAccount.add(objAccount);
        }
        insert lstAccount;
        
        Id batchJobId = Database.executeBatch(new SL_BatchAutoCreateAccountReporting());
        
        List<String> lstOppRecTypeName = new List<String>{'Add-On - Standard', 'Add-On - Variable Price','Ancillary','Ancillary Renewal','BCBSMA - Senior Markets','Dental','Dental Renewal',
            												'	Medical','	Medical Renewal','Small Group New Business','Small Group Off Anniversary','Small Group Renewal','Stop Loss','Stop Loss Renewal','Vision','	Vision Renewal'};
            
        List<RecordType> smallRecType = new List<RecordType>();
        List<RecordType> largeRecType = new List<RecordType>();
        for(RecordType objRecordType : [Select Id, Name From RecordType Where SobjectType = 'Opportunity' AND Name IN : lstOppRecTypeName]) {
            if(objRecordType.Name.contains('Small Group'))
                smallRecType.add(objRecordType);
            else
                largeRecType.add(objRecordType);
        }
        
        List<Opportunity> lstOpportunity = new List<Opportunity>{
            new Opportunity(Name = 'Test Opp1', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Closed/Won', AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp2', Effective_Date__c = Date.newInstance(system.today().Year(), 4, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[1].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp3', Effective_Date__c = Date.newInstance(system.today().Year()+1, 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp4', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[3].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp5', Effective_Date__c = Date.newInstance(system.today().Year()-1, 1, 2),StageName = 'Value Proposition',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
             new Opportunity(Name = 'Test Opp6', Effective_Date__c = Date.newInstance(system.today().Year(), 11, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[5].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp7', Effective_Date__c = Date.newInstance(system.today().Year(), 12, 2),StageName = 'Closed/Lost',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[4].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp8', Effective_Date__c = Date.newInstance(system.today().Year(), 7, 2),StageName = 'Closed/Won',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[7].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp9', Effective_Date__c = Date.newInstance(system.today().Year()+1, 2, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id, CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp10', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[2].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp11', Effective_Date__c = Date.newInstance(system.today().Year()-1, 2, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id,CloseDate = Date.newInstance(system.today().Year()+1, 6, 4) ),
            new Opportunity(Name = 'Test Opp12', Effective_Date__c = Date.newInstance(system.today().Year()-1, 1, 2),StageName = 'Closed/Lost',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[0].Id,CloseDate = Date.newInstance(system.today().Year(), 6, 4) ),
            new Opportunity(Name = 'Test Opp13', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[3].Id,CloseDate = Date.newInstance(system.today().Year(), 6, 4) ),
            new Opportunity(Name = 'Test Opp14', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[0].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = largeRecType[6].Id ,CloseDate = Date.newInstance(system.today().Year(), 6, 4)),
                new Opportunity(Name = 'Test Opp15', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 1),StageName = 'Needs Analysis',AccountId = lstAccount[1].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[1].Id ,CloseDate = Date.newInstance(system.today().Year(), 6, 4)),
                new Opportunity(Name = 'Test Opp16', Effective_Date__c = Date.newInstance(system.today().Year(), 1, 2),StageName = 'Needs Analysis',AccountId = lstAccount[1].Id,
                                            Net_Dental_Eligibles__c = 1, Net_Medical_Eligibles__c = 1, Net_Senior_Eligibles__c = 1, RecordTypeId = smallRecType[1].Id ,CloseDate = Date.newInstance(system.today().Year(), 6, 4))
        };
        insert lstOpportunity;
        
        
        List<Quote> lstQuote = new List<Quote>{
            new Quote(Name = 'Test Quote1', Synced__c = true, OpportunityId = lstOpportunity[0].Id),
                new Quote(Name = 'Test Quote2', Synced__c = true, OpportunityId = lstOpportunity[8].Id),
                new Quote(Name = 'Test Quote3', Synced__c = true, OpportunityId = lstOpportunity[4].Id),
                new Quote(Name = 'Test Quote4', Synced__c = true, OpportunityId = lstOpportunity[10].Id),
                new Quote(Name = 'Test Quote5', Synced__c = true, OpportunityId = lstOpportunity[11].Id),
                new Quote(Name = 'Test Quote6', Synced__c = true, OpportunityId = lstOpportunity[14].Id),
                new Quote(Name = 'Test Quote7', Synced__c = true, OpportunityId = lstOpportunity[15].Id)
        };
        
       insert lstQuote;
        
        List<Quote_Plan__c> lstQuotePlan = new List<Quote_Plan__c>{
            new Quote_Plan__c(Name = 'Test QP1', Product_Line__c = 'Medical',Quote_Plan_Type__c='Sold',Quote__c = lstQuote[0].Id),
                new Quote_Plan__c(Name = 'Test QP2', Product_Line__c = 'Dental',Quote_Plan_Type__c='Sold',Quote__c = lstQuote[0].Id),
                new Quote_Plan__c(Name = 'Test QP3', Product_Line__c = 'Senior',Quote_Plan_Type__c='Current',Quote__c = lstQuote[0].Id),
                new Quote_Plan__c(Name = 'Test QP3', Product_Line__c = 'Senior',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[0].Id),
                new Quote_Plan__c(Name = 'Test QP4', Product_Line__c = 'Medical',Quote_Plan_Type__c='Quoting',Quote__c = lstQuote[1].Id),
                new Quote_Plan__c(Name = 'Test QP5', Product_Line__c = 'Dental',Quote_Plan_Type__c='Quoting',Quote__c = lstQuote[1].Id),
                new Quote_Plan__c(Name = 'Test QP6', Product_Line__c = 'Senior',Quote_Plan_Type__c='Current',Quote__c = lstQuote[1].Id),
                new Quote_Plan__c(Name = 'Test QP7', Product_Line__c = 'Senior',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[1].Id),
                new Quote_Plan__c(Name = 'Test QP4', Product_Line__c = 'Medical',Quote_Plan_Type__c='Current',Quote__c = lstQuote[3].Id),
                new Quote_Plan__c(Name = 'Test QP5', Product_Line__c = 'Dental',Quote_Plan_Type__c='Current',Quote__c = lstQuote[3].Id),
                new Quote_Plan__c(Name = 'Test QP6', Product_Line__c = 'Senior',Quote_Plan_Type__c='Current',Quote__c = lstQuote[3].Id),
                new Quote_Plan__c(Name = 'Test QP7', Product_Line__c = 'Senior',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[3].Id),
                new Quote_Plan__c(Name = 'Test QP4', Product_Line__c = 'Medical',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[4].Id),
                new Quote_Plan__c(Name = 'Test QP5', Product_Line__c = 'Dental',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[4].Id),
                new Quote_Plan__c(Name = 'Test QP6', Product_Line__c = 'Senior',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[4].Id),
                new Quote_Plan__c(Name = 'Test QP7', Product_Line__c = 'Senior',Quote_Plan_Type__c='Pre-IBR',Quote__c = lstQuote[4].Id),
                new Quote_Plan__c(Name = 'Test QP6', Product_Line__c = 'Medical',Quote_Plan_Type__c='Quoting',Quote__c = lstQuote[5].Id),
                new Quote_Plan__c(Name = 'Test QP7', Product_Line__c = 'Medical',Quote_Plan_Type__c='Quoting',Quote__c = lstQuote[6].Id)
        };
        insert lstQuotePlan;
        
    }
    
    @istest
    static void autoCreateAccountReportingTest() {
        test.startTest();
            SL_BatchAutoCreateOppRepSchedular m = new SL_BatchAutoCreateOppRepSchedular();
            String sch = '0 0 23 * * ?';
            String jobID = system.schedule('Auto Create Opportunity Reporting', sch, m);
            m.execute(null);
        test.stopTest();
        system.assertEquals(21, [Select Id From Account_Reporting__c].size());
        m.execute(null);
    }
}