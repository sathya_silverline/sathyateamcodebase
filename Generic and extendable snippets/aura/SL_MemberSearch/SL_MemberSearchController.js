({
	doInit : function(component, event, helper) {
		
		component.set("v.Email", '');
		component.set("v.Phone", '');
		component.set("v.FirstName", '');
		component.set("v.LastName", '');
		component.set("v.PersonBirthdate", '');
		component.set("v.SSNLast4__pc", '');

		var urlForm = window.location.href;
		var url = new URL(urlForm);
		var ani = helper.getParameterByNames('ani', urlForm);
		var dnis = helper.getParameterByNames('dnis', urlForm);

		var firstName = helper.getParameterByNames('firstName', urlForm);
		var lastName = helper.getParameterByNames('lastName', urlForm);
		var dob = helper.getParameterByNames('dob', urlForm);
		var ssn = helper.getParameterByNames('ssn', urlForm);
		var email = url.searchParams.get('email');
		var ref = helper.getParameterByNames('ref', urlForm);

		if((ani == undefined || ani == null || ani == '' || ani == 'null') && (firstName == undefined || firstName == null || firstName == '' || firstName == 'null') && (lastName == undefined || lastName == null || lastName == '' || lastName == 'null') && (dob == undefined || dob == null || dob == '' || dob == 'null') && (ssn == undefined || ssn == null || ssn == '' || ssn == 'null'))
			return;

		if(ani != undefined && ani != null && ani != '' && ani != 'null')
			component.set("v.Phone", ani);
		if(firstName != undefined && firstName != null && firstName != '' && firstName != 'null')
			component.set("v.FirstName", firstName);
		if(lastName != undefined && lastName != null && lastName != '' && lastName != 'null')
			component.set("v.LastName", lastName);
		if(dob != undefined && dob != null && dob != '' && dob != 'null')
			component.set("v.PersonBirthdate", dob);
		if(ssn != undefined && ssn != null && ssn != '' && ssn != 'null')
			component.set("v.SSNLast4__pc", ssn);

		component.set("v.isInitSearch", true);
		var action = component.get("c.getResult");
		action.setParams({
			"component": component,
			"event": event,
			"helper": helper
		});
        action.setCallback(this, function(response) {
			var state = response.getState();
        });
        $A.enqueueAction(action);

	},

	getResult : function(component, event, helper) {
		var ssnIsValid = helper.ssnChange(component, event, helper);
		if(ssnIsValid == false || component.get("v.isValidDate") == false)
			return false;

		helper.getRecords(component, event, helper, 0);
	},

	lookupChange : function(component, event, helper) {

		if(!$A.util.isUndefinedOrNull(event.getParam("value").Id))
			component.set("v.Employee_ID_Issuer__c", event.getParam("value").Id);
		else
			component.set("v.Employee_ID_Issuer__c", "");
	},

	clearSearch : function(component, event, helper){
		component.find("email").set("v.value", "");
		component.find("firstName").set("v.value", "");
		component.find("lastName").set("v.value", "");
		component.set("v.Phone", "");
		component.set("v.PersonBirthdate", "");
		component.find("last4SSN").set("v.value", "");
		component.set("v.Employee_ID_Issuer__c", "");
		helper.ssnChange(component, event, helper);
	},

	sortByNameAsc : function(component, event, helper){
		helper.getRecords(component, event, helper, 0);
	},
	sortByNameDesc : function(component, event, helper){
		helper.getRecords(component, event, helper, 1);
	},
	sortByDOBAsc : function(component, event, helper){
		helper.getRecords(component, event, helper, 2);
	},
	sortByDOBDesc : function(component, event, helper){
		helper.getRecords(component, event, helper, 3);
	},

	createNewMember : function(component, event, helper){
		component.set("v.createNewMember", true);
	},

	createMemberToMemberSearch : function(component, event, helper){
		helper.closeModal(component, event, event.getParam("closeCreateMemberModal"));
	},

	keyupBirthdate : function(component, event, helper) {
		var cmpBirthdate = component.find("birthdate");
		var dateformat = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;
		var cmpBirthdateValue = event.getParam("value");
		if(!cmpBirthdateValue.match(dateformat) && cmpBirthdateValue != ''){
			cmpBirthdate.set("v.errors", [{message:"Please enter in MM/DD/YYYY format."}]);
			component.set("v.isValidDate", false);
		}
		else{
			cmpBirthdate.set("v.errors", null);
			component.set("v.isValidDate", true);
		}
	},

	moveToDetailPage : function(component, event, helper) {
		var recordId = event.target.title;
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		"recordId": recordId,
		"slideDevName": "detail"
		});
		navEvt.fire();
	},

	formkeyPress : function(component, event, helper) {
		if (event.keyCode === 13)
			helper.getRecords(component, event, helper, 0);
	},
	
	ssnChange : function(component, event, helper) {
		helper.ssnChange(component, event, helper);
	},

	onMouseOverClearButton : function(component, event, helper) {
	},

	onMouseOutClearButton : function(component, event, helper) {
	},

	ssnValid : function(component, event, helper) {
		var ssnFormat = /^\d{4}$/;
		var ssnComp = component.find("last4SSN");
		var ssnValue = ssnComp.get("v.value");
        if(ssnValue.match(ssnFormat) || ssnValue == null || ssnValue == ''){
			ssnComp.setCustomValidity("");
			ssnComp.reportValidity();
        }
	},

	phoneValid : function(component, event, helper) {
        var phoneFormat = /^\d*$/;
        var phoneValue = component.get("v.Phone");
        var phoneComp = component.find("phone");
        if(phoneValue.match(phoneFormat) || phoneValue == null || phoneValue == ''){
			phoneComp.setCustomValidity("");
			phoneComp.reportValidity();
		}
	},

	// locationChange : function(component, event, helper) {

	// 	var workspaceAPI = component.find("workspace");
		
	// 	var urlForm = window.location.href;
	// 	var url = new URL(urlForm);
	// 	var ani = helper.getParameterByNames('ani', urlForm);
	// 	var dnis = helper.getParameterByNames('dnis', urlForm);

	// 	workspaceAPI.openSubtab({
	// 		url: "/lightning/n/Member_Lookup?ani=" + ani + "&dnis=" + dnis,
	// 		focus: true
	// 	})
    //     .catch(function(error) {
    //         console.log(error);
    //     });
	// },
})