({
	getRecords : function(component, event, helper, sortOrder) {

		component.set("v.searching", true);
		component.set("v.sortOrder", sortOrder);
		var hContainer = component.find("headerContainer");
		var bContainer = component.find("bodyContainer");
		$A.util.addClass(hContainer, 'slds-hide');
		$A.util.addClass(bContainer, 'slds-hide');

		var FirstName = component.get("v.FirstName").trim();
		var LastName = component.get("v.LastName").trim();
		var Phone = component.get("v.Phone").trim();
		var Email = component.get("v.Email").trim();
		var PersonBirthdate = component.get("v.PersonBirthdate").trim();
		var SSNLast4__pc = component.get("v.SSNLast4__pc").trim();
		var Employee_ID_Issuer__c = component.get("v.Employee_ID_Issuer__c").trim();

		var action = component.get("c.getResults");
		action.setParams({
			"FirstName": (((FirstName.length >= 1) == true) ? FirstName : ""),
			"LastName": (((LastName.length >= 1) == true) == true ? LastName : ""),
			"Phone": ($A.util.isUndefinedOrNull(Phone) != true ? Phone : ""),
			"Email": ($A.util.isUndefinedOrNull(Email) != true ? Email : ""),
			"PersonBirthdate": ($A.util.isUndefinedOrNull(PersonBirthdate) != true ? PersonBirthdate : ""),
			"SSNLast4": ($A.util.isUndefinedOrNull(SSNLast4__pc) != true ? SSNLast4__pc : ""),
			"Employee_ID_Issuer__c": ($A.util.isUndefinedOrNull(Employee_ID_Issuer__c) != true ? Employee_ID_Issuer__c : ""),
			"sortOrder": sortOrder
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var result = response.getReturnValue();
				component.set("v.results", result);

				if(result.length == 0 || result.length > 100){
					component.set("v.isError", true);
					component.set("v.errorMessage", "Has Error");
					if(result.length > 100)
						component.set("v.isLimitExceed", true);
					else
						component.set("v.isLimitExceed", false);
				}
				else{
					component.set("v.isError", false);
					component.set("v.isLimitExceed", false);
					component.set("v.errorMessage", "");

					if(component.get("v.isInitSearch")){
						component.set("v.Phone", '');
						component.set("v.isInitSearch", false);
					}
				}
			}
			else{
				component.set("v.results", null);
				component.set("v.isError", true);
				component.set("v.errorMessage", "Has Error");
			}
			
			component.set("v.searching", false);
			$A.util.removeClass(hContainer, 'slds-hide');
			$A.util.removeClass(bContainer, 'slds-hide');
        });
        $A.enqueueAction(action);
	},

	closeModal : function(component, event, isClose){
		component.set("v.createNewMember", !isClose);
	},

	firstNameChange : function(component, event, helper) {
		var fnComp = component.find("firstName");
		var fnValue = fnComp.get("v.value");
        if(fnValue != null && fnValue != '' && fnValue.length < 3){
			fnComp.setCustomValidity("Please enter minimum 3 characters");
			fnComp.reportValidity();
			return false;
        }
        else{
			fnComp.setCustomValidity("");
			fnComp.reportValidity();
			return true;
		}
	},
	
	lastNameChange : function(component, event, helper) {
		var lnComp = component.find("lastName");
		var lnValue = lnComp.get("v.value");
        if(lnValue != null && lnValue != '' && lnValue.length < 3){
			lnComp.setCustomValidity("Please enter minimum 3 characters");
			lnComp.reportValidity();
			return false;
        }
        else{
			lnComp.setCustomValidity("");
			lnComp.reportValidity();
			return true;
		}
	},
	
	ssnChange : function(component, event, helper) {
		var ssnFormat = /^\d{4}$/;
		var ssnComp = component.find("last4SSN");
		var ssnValue = ssnComp.get("v.value");
        if(ssnValue != null && ssnValue != '' && !ssnValue.match(ssnFormat)){
			ssnComp.setCustomValidity("Please enter a 4 digit number");
			ssnComp.reportValidity();
			return false;
        }
        else{
			ssnComp.setCustomValidity("");
			ssnComp.reportValidity();
			return true;
		}
	},

	phoneChange : function(component, event, helper) {
		var phoneFormat = /^\d*$/;
        var phoneValue = component.get("v.Phone");
        var phoneComp = component.find("phone");
        if(phoneValue != null && phoneValue != '' && !phoneValue.match(phoneFormat)){
			phoneComp.setCustomValidity("Please enter valid number");
			phoneComp.reportValidity();
			return false;
        }
        else{
			phoneComp.setCustomValidity("");
			phoneComp.reportValidity();
			return true;
		}
	},

	getParameterByNames: function(name, url) {
        if(!url){
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),results = regex.exec(url);
        if(!results){
            return null;
        }
        if(!results[2]){
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
})