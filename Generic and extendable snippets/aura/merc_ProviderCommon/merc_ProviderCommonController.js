({
    commonInit: function(component, event, helper) {
        
    },

    navigateToLinkHref : function(component, event, helper) {
        event.preventDefault();
        var url = event.currentTarget.getAttribute('href');
        helper.navigateToURL(component, url);
        event.stopPropagation();
    },
})
