({   
    /**
     * Show toast helper.
     */
    showToast : function(component, severity, message, title, key, duration, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration: duration,
            key: key,
            type: severity,
            mode: mode
        });
        toastEvent.fire();
    },

    /**
     * Server action wrapper method
     */
    serverAction : function(component, methodName, options) {
        var helper = this;
        var showToast = this.showToast;
        // define call defaults... To be overwritten by options if key is provided
        var defaults = {
            success: function(response) {
            },
            incomplete: function(response) {
                showToast(component, 'warning', 'Your request could not be completed, please try again.', null, 'info_alt', '5000', 'pester');
                self.log("Incomplete call:" + methodName);
            },
            error: function(response) {
                var errors = response.getError();
                var allErrors = helper.gatherAllErrors(errors);
                console.error(allErrors);               
                allErrors.forEach(function(errorMessage) {
                    showToast(component, 'warning', errorMessage, 'Error', 'info_alt', '5000', 'pester');
                });
            },
            whenDone: function(response) {}, // finally is a keyword that messes something up... Use whenDone instead
            storable: false,
            abortable: false,
            params: false
        };
        
        /* Object.assign polyfill */
        if (!Object.assign instanceof Function) {
            Object.defineProperty(Object, "assign", {
                value: function assign(target, varArgs) { // .length of function is 2
                  'use strict';
                  if (target == null) { // TypeError if undefined or null
                    throw new TypeError('Cannot convert undefined or null to object');
                  }
            
                  var to = Object(target);
            
                  for (var index = 1; index < arguments.length; index++) {
                    var nextSource = arguments[index];
            
                    if (nextSource != null) { // Skip over if undefined or null
                      for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                          to[nextKey] = nextSource[nextKey];
                        }
                      }
                    }
                  }
                  return to;
                },
                writable: true,
                configurable: true
            });
        }

        var opt = Object.assign({}, defaults, options);

        var action = component.get(methodName);
        
        if (opt.params){
            action.setParams(opt.params);
        }
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid()) {
                switch(state) {
                    case "SUCCESS":
                        opt.success(response);
                        break;
                    case "INCOMPLETE":
                        opt.incomplete(response);
                        break;
                    case "ERROR":
                        opt.error(response);
                        break;
                    default:
                        break;
                }
            }

            opt.whenDone(response);
        });
        
        if (opt.storable) {
            action.setStorable();
        }
        
        if (opt.abortable) {
            action.setAbortable();
        }
        
        $A.enqueueAction(action);   
    },
    
    gatherAllErrors : function(errors) {
        var allErrors = [];
        if (errors && errors[0]) {
            if (errors[0].fieldErrors) {
                if (errors[0].fieldErrors.length > 0) {
                    errors[0].fieldErrors.forEach(function(error) {
                        if(error.message) {
                            allErrors.push(error.message);
                        }
                    });
                }
            }
            if (errors[0].pageErrors) {
                if (errors[0].pageErrors.length > 0) {
                    errors[0].pageErrors.forEach(function(error) {
                        if (error.message) {
                            allErrors.push(error.message);
                        }
                    });
                }
            }
            if (errors[0] && errors[0].message) {
                allErrors.push(errors[0].message);
            }
        } else {
            allErrors.push('An unknown error has occurred.');
        }
        return allErrors
    },

    navigateToURL : function(component, url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

    getUrlParam : function(sParam){
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVars = sPageURL.split('&');
        var sParamName;
        
        for(var i = 0; i < sURLVars.length; i++){
            sParamName = sURLVars[i].split('=');

            if(sParamName[0] === sParam){
                return sParamName[1] === undefined ? true : sParamName[1];
            }
        }
    },

    getObjectFromId : function(arr, id){
        id = parseInt(id, 10);
        var result = arr.filter(function(element) {
            return element.id === id;
        });

        return result[0];
    },

    dateCompare: function(date1, date2) {
        if(date1.getFullYear() !== date2.getFullYear()){
            return date1.getFullYear() - date2.getFullYear();
        }else{
            if(date1.getMonth() !== date2.getMonth()){
                return date1.getMonth() - date2.getMonth();
            }else{
                return date1.getDate() - date2.getDate();
            }
        }
    },

    parseDate: function(dateStr) {
        var helper = this;

        if(dateStr){
            var newDate = new Date(dateStr);
            if(helper.isDateValid(newDate) && helper.isExpectedDate(newDate, dateStr)){
                return newDate;
            }
        }

        return null;
    },

    getStoredCookieValue: function(cookieName) {
        var returnValue = null;
        var term = cookieName+"=";
        if(document.cookie.length > 0){
            var offset = document.cookie.indexOf(term);
            if(offset !== -1){
                offset += term.length;

                var end = document.cookie.indexOf(";", offset);

                if(end === -1){
                    end = document.cookie.length;
                }

                returnValue = unescape(document.cookie.substring(offset, end));
            }
        }

        return returnValue;    
    },
    
    saveValueToCookie: function(cookieName,cookieValue) {
        document.cookie = cookieName+"="+cookieValue;
    },
    
    clearStoredCookie: function(cookieName) {
        document.cookie = cookieName+"= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    },
})