({
    doInit : function(component, event, helper) {
        
        // Params is an JSON structure which will use to pass method parameter as shown below.
        let params = {
            //method parameter name1 : value,
            //method parameter name2 : value,
        }
        
        helper.serverAction(component, 'c.fetchAllAccounts', {
            success: function(response){
                let allAccounts = response.getReturnValue();
            },
            error: function(response){
                var errors = response.getError();
                var allErrors = helper.gatherAllErrors(errors);
                console.error(allErrors);
            },
            whenDone: function(){
                //After Calling a Server-Side action you can add functions to execute. 
                //Ex : Closing loading symbol after action call!!
            },
            params: params
        });
        
        helper.showToast(component, 'success', 'Previews updated successfully', null, null, 5000, 'dismissible');
        
        helper.showToast(component, 'warning', 'URL Param articleId is missing from the URL', 'Error', 'info_alt', '5000', 'sticky');
        
    },
})