public class SL_MemberSearchController {
    private static final Integer RESULT_LIMIT = 110;

    // Note: The EmployeeIDIssuer paramater (previously named CompanyOrganization)
    // is unused, but it's passed in from SL_MemberSearch.cmp in an attribute
    // named selectedLookUpRecord which is then used in the helper. We're trying
    // to get clarification on where this is used
    @AuraEnabled
    public static List<Account> getResults(String FirstName, String LastName, 
                                        String Phone, String PersonBirthdate, 
                                        String SSNLast4, String EmployeeIDIssuer,
                                        String Email, Integer sortOrder) {

        // Check which fields are set, to determine if we should drop into SOQL vs SOSL
        Boolean hasSSNLast4 = String.isNotBlank(SSNLast4);
        Boolean hasPersonBirthdate = String.isNotBlank(PersonBirthdate);
        Boolean hasFirstName = String.isNotBlank(FirstName);
        Boolean hasLastName = String.isNotBlank(LastName);
        Boolean hasPhone = String.isNotBlank(Phone);
        Boolean hasEmail = String.isNotBlank(Email);
        Boolean hasNameFields = hasFirstName || hasLastName;

        Boolean hasExactFields = hasSSNLast4 || hasPersonBirthdate;
        Boolean hasFuzzyFields = hasNameFields || hasEmail || hasPhone;

		// Returned field names from SOSL/SOQL query
        List<String> fieldsToQuery = new List<String>{
            'Id', 'Name', 'PersonBirthdate', 'SSNLast4__pc', 'Phone',
            'PersonHomePhone', 'PersonMobilePhone', 'PersonOtherPhone',
            'PersonEmail'
        };

        // Build the "WHERE x = y" filters for exact match searching. When the query
        // is performed, words prefixed with a colon are magically interpolated from
        // variables with the same name in the current scope
        List<String> filters = new List<String>();
        if(hasSSNLast4) {
            filters.add('SSNLast4__pc = :SSNLast4');
        }

        if(hasPersonBirthdate) {
            List<String> dateFields = PersonBirthdate.split('-');
            Date birthdate = Date.newInstance(
                Integer.valueOf(dateFields.get(0)),
                Integer.valueOf(dateFields.get(1)),
                Integer.valueOf(dateFields.get(2)));
            filters.add('PersonBirthdate = :birthdate');
        }

        // Filter on only Member account types
        filters.add('RecordType.Name = \'Member\'');
                 
        // Results of Query
        List<Account> personAccounts = new List<Account>();

        // If we're querying only exact match fields, then we we have to use SOQL.
        // There's no way in SOSL to do "FIND * IN Account WHERE email = exact"
        // because you have to provide at least one search character to SOSL
        if(hasExactFields && !hasFuzzyFields) {
            String query = String.format(
                'SELECT {0} FROM Account WHERE {1} LIMIT {2}',
                new String[]{
                    String.join(fieldsToQuery, ', '),
                    String.join(filters, ' AND '),
                    String.valueOf(RESULT_LIMIT)
                }
            );

          	personAccounts = Database.query(query);

        // If we're searching on at least one fuzzy field, then we can pass those to SOSL's search
        // index (FIND X in...) which supports fuzzy and case insensitive matching on groups of
        // fields
        } else {
            // Start building fuzzy field search list with escaped values, then
            // remove any we don't have
            Set<String> fuzzyFieldValues = new Set<String>{
              SL_MemberSearchController.escapeSosl(FirstName),
              SL_MemberSearchController.escapeSosl(LastName),
              SL_MemberSearchController.escapeSosl(Phone),
              SL_MemberSearchController.escapeSosl(Email)
            };
            fuzzyFieldValues.remove('');
            
            // There's something I don't understand about SOSL, it supports * for wildcard,
            // but sometimes you don't need them. I don't know when. This turns
            // ["Bob", "Toronto"] into "*Bob* AND *Toronto*", surrounding each term with
            // single quotes
            String fuzzies = '*' + String.join(new List<String>(fuzzyFieldValues), '* AND *') + '*';
            
            String fieldSelection = '';
            if(hasNameFields && !hasPhone && !hasEmail) {
                fieldSelection = 'IN NAME FIELDS';
            } else if(hasPhone && !hasEmail && !hasNameFields) {
                fieldSelection = 'IN PHONE FIELDS';
            } else if(hasEmail && !hasPhone && !hasNameFields) {
                fieldSelection = 'IN EMAIL FIELDS';
            }

            String query = String.format(
                'FIND \'{\'{0}\'}\' {1} RETURNING Account ({2} WHERE {3} LIMIT {4})',
                new String[]{
                    fuzzies,
                    fieldSelection,
                    String.join(fieldsToQuery, ', '),
                    String.join(filters, ' AND '),
                    String.valueOf(RESULT_LIMIT)
                }
            );

            // SOSL queries return an array of arrays, where each top level array is the results
            // for that object type. Since we're only querying for Person Account object types,
            // we only need the first (and only) query group returned
          	personAccounts = Search.query(query)[0];
        }

        List<Account> resultSet = new List<Account>();

        // Normalize phone number in search results
        for(Account obj: personAccounts) {
            if(String.isBlank(obj.Phone)){
                if(obj.PersonHomePhone != null && obj.PersonHomePhone.contains(Phone))
                    obj.Phone = obj.PersonHomePhone;
                else if(obj.PersonMobilePhone != null && obj.PersonMobilePhone.contains(Phone))
                    obj.Phone = obj.PersonMobilePhone;
                else if(obj.PersonOtherPhone != null && obj.PersonOtherPhone.contains(Phone))
                    obj.Phone = obj.PersonOtherPhone;
            }

            resultSet.add(obj);
        }

        /* Original sort mapping from Silverline, when we could sort on SOQL fields
        Map<Integer, String> indexToSortOrder = new Map<Integer, String>{
            0 => ' ORDER BY Name ASC',
            1 => ' ORDER BY Name DESC',
            2 => ' ORDER BY PersonBirthdate ASC',
            3 => ' ORDER BY PersonBirthdate DESC'
        };
		*/
		
        // In-memory sorting :(
        List<Account> sortedResults = new List<Account>();

        // TODO: This line doesn't work, can't figure out how to declare an enum type here
        //GR_SObjectComparator.Direction direction;

        if(sortOrder == 3) {
            sortedResults = GR_SObjectComparator.sortList(
                resultSet, 'PersonBirthdate', 'Date', GR_SObjectComparator.Direction.DESCENDING
            );
        } else if(sortOrder == 2) {
            sortedResults = GR_SObjectComparator.sortList(
                resultSet, 'PersonBirthdate', 'Date', GR_SObjectComparator.Direction.ASCENDING
            );
        } else if(sortOrder == 1) {
            sortedResults = GR_SObjectComparator.sortList(
                resultSet, 'Name', 'String', GR_SObjectComparator.Direction.DESCENDING
            );
        // Default to (first) name ascending
        } else {
            sortedResults = GR_SObjectComparator.sortList(
                resultSet, 'Name', 'String', GR_SObjectComparator.Direction.ASCENDING
            );
        }

        return sortedResults;
    }

    // Believe it or not, there's no built in method for this, and variable interpolation in the
    // SOSL String (:variableName) doesn't seem to work. Note that searching for SOSL join words
    // like AND or OR may break the query, because the string is built raw.
    private static String escapeSosl(String input) {
      String escapeCharacters = '[\\?&\\|!{}\\[\\]\\(\\)\\^~\\*:\\\"\'\\+-]';
      return input.replaceAll(escapeCharacters, '\\\\$0');
    }
}