/**=====================================================================
* Silverline
* Name: SL_AccountController
* Description: fetch list of all accounts and display in the community page.
* Created Date: 08th, July-2019
* Modified Date:
* Created By: Anil Mohan
*
=====================================================================*/
public with sharing class SL_AccountController {

    @auraEnabled 
    public static List<Account> fetchAllAccounts() {
        
        return [SELECT Id,Name from Account];
    }
}