public class SL_DynamicLookupController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        List < sObject > returnList = new List < sObject > ();
  
        // Because we encrypt certain fields, we have to use sosl, not soql, to search
        // More information: https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_sosl_find.htm
		String soslQuery = 'Find :searchKeyWord RETURNING '+ObjectName+'(Id, Name WHERE RecordType.Name = \'Business\')';
        
        List<List<sObject>> searchList = Search.query(soslQuery);
        sObject[] searchObjects = (sObject[])searchList[0];
        
        for (sObject obj: searchObjects) {
            returnList.add(obj);
        }
        return returnList;
    }
}