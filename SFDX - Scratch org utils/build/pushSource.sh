#!/bin/bash

set -e
trap cleanup EXIT
function cleanup {
    if [ $? != 0 ]; then
        echo "Cleaning up Scratch Org"
        sfdx force:org:delete --targetusername scratchOrg --noprompt
        exit 1;
    else
        exit 0;
    fi
}
sfdx force:source:push -u scratchOrg --wait 10
