#!/bin/bash

set -e
trap cleanup EXIT
function cleanup {
    if [ $? != 0 ]; then
        echo "Cleaning up Scratch Org"
        sfdx force:org:delete --targetusername scratchOrg --noprompt
        exit 1;
    else
        exit 0;
    fi
}
LATESTMAIN=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("Main@"))] | sort | .[-1]')

sfdx force:package:install --package $LATESTMAIN --noprompt --publishwait 10 --wait 10