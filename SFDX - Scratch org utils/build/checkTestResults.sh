#!/bin/bash

set +e
sfdx force:apex:test:run --wait 10 --json > testresults.json
set -e

#output results
TESTRUNID=$(cat testresults.json | jq '.result.summary.testRunId' -r)
sfdx force:apex:test:report -i $TESTRUNID

#assert status
TESTSTATUS=$(cat testresults.json | jq '.result.summary.outcome' -r)
if [ "$TESTSTATUS" == "Failed" ]; then
    sfdx force:org:delete --targetusername scratchOrg --noprompt
    exit 1;
else
    exit 0;
fi
