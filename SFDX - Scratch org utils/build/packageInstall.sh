#!/bin/bash

MAX_TRIES=4

for (( i=0; i<$MAX_TRIES; i++ ))
    do
        if [ $i -ne 0 ]; then
            echo "Sleeping for 5m"
            sleep 5m
        fi

        sfdx force:package:install --package "Mercury@0.1.0-1" --noprompt --publishwait 10 --wait 10
        if [ $? -eq 0 ]; then
            echo "exit 0"
            exit 0;
        fi
    done
echo "Too many unsuccessful tries"
exit 1;
