#!/bin/bash

JSONCOUNT="$(ls -lr data/*-plan.json | wc -l)"
if [ ${JSONCOUNT} -gt 0 ] ; then
    echo "Importing Data..."
    for file in data/*-plan.json ; do
        echo "Importing $file"
        sfdx force:data:tree:import --plan $file
    done
fi