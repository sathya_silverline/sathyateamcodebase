#!/bin/bash

set -e
trap cleanup EXIT
function cleanup {
    if [ $? != 0 ]; then
        echo "Cleaning up Scratch Org"
        sfdx force:org:delete --targetusername scratchOrg --noprompt
        exit 1;
    else
        exit 0;
    fi
}
LATESTCORE=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("intl-core@"))] | sort | .[-1]')
LATESTCOMMON=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("intl-common@"))] | sort | .[-1]')
LATESTLOGGING=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("Logging@"))] | sort | .[-1]')
LATESTSALES=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("intl-sales@"))] | sort | .[-1]')
LATESTPARDOT=$(cat sfdx-project.json | jq '.packageAliases' | jq -r '[keys[] | select(contains("Pardot@"))] | sort | .[-1]')

echo "Installing Pardot..."
sfdx force:package:install --package $LATESTPARDOT --noprompt --publishwait 20 --wait 20

echo "Installing intl-core..."
sfdx force:package:install --package $LATESTCORE --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing intl-common..."
sfdx force:package:install --package $LATESTCOMMON --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing intl-sales..."
sfdx force:package:install --package $LATESTSALES --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing Logging..."
sfdx force:package:install --package $LATESTLOGGING --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544