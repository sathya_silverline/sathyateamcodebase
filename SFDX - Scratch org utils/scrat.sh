#!/bin/bash

set -e

HASALIAS=false
while [ ${#} -gt 0 ] ; do
    if [ $1 = "-a" ] ; then
        shift
        ALIAS=$1
        HASALIAS=true
    else
        echo "Unknown Parameter: use is ./scratch.sh -a <Alias>"
        echo "Where <Alias> is replaced with the alias you want to use for the scratch org"
        exit 1
    fi
    shift
done

if [ "$HASALIAS" = false ] ; then
    echo "Requires -a followed by an alias for the scratch org"
    exit 1
fi

cd mercury
echo "Creating scratch org $ALIAS..."
sfdx force:org:create -s -f ./config/project-scratch-def.json -a $ALIAS -d 14

echo "Installing Pardot to Scratch Org $ALIAS..."
sfdx force:package:install --package Pardot@4.42.0.1 --noprompt --publishwait 20 --wait 20

echo "Installing intl-core to Scratch Org $ALIAS..."
sfdx force:package:install --package intl-core@1.2.0-1 --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing intl-common to Scratch Org $ALIAS..."
sfdx force:package:install --package intl-common@1.2.0-9 --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing intl-sales to Scratch Org $ALIAS..."
sfdx force:package:install --package intl-sales@1.2.0-15 --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing Logging Package to Scratch Org $ALIAS..."
sfdx force:package:install --package Logging@1.1.0-1 --noprompt --publishwait 10 --wait 10 -k MoreWithLess5544

echo "Installing Main Package to Scratch Org $ALIAS..."
sfdx force:package:install --package "Main@0.1.0-32" --noprompt --publishwait 10 --wait 10

echo "Deploy StandardValueSet for LeadStatus to $ALIAS..."
sfdx force:source:deploy -p "../unpackaged/standardValueSets"

echo "Pushing Local Changes to Scratch Org $ALIAS..."
sfdx force:source:push -u $ALIAS
JSONCOUNT="$(ls -lr data/*-plan.json | wc -l)"
if [ ${JSONCOUNT} -gt 0 ] ; then
    echo "Importing Data..."
    for file in data/*-plan.json ; do
        echo "Importing $file"
        sfdx force:data:tree:import --plan $file
    done
fi
echo "Building Community in $ALIAS..."
cd ../community
sfdx force:source:push -f -u $ALIAS
cd ../mercury
echo "Opening Scratch Org..."
sfdx force:org:open -u $ALIAS
echo "Running Tests..."
sfdx force:apex:test:run -u $ALIAS --wait 10
echo "Lint Lightning..."
sfdx force:lightning:lint ./force-app/main/default/aura/

echo ""
echo "Post Scratch Org Steps:"
echo "Setup > Feature Settings > Communities > All Communities"
echo "  For Mercury community, open Builder"
echo "  Click Publish"
echo ""
echo "In Builder, go to Settings > General"
echo "  Click the link for Mercury Profile under the Guest User Profile heading"
echo "  Click Edit on the Default Community related list (bottom of page)"
echo "  Change Community dropdown to Mercury and Save"
echo ""
echo "Complete setup for sample Community user"
echo "  Go to Setup > Roles"
echo "    Create a new top-level role (role name does not matter)"
echo "    Assign the user record \"User User\" to the new role"
echo "  Go to Contacts and open the John Doe record"
echo "    Click the Enable Customer User button"
echo "      Change User License to \"Customer Community Plus Login\""
echo "      Change Profile to \"Customer Community Plus Login User\" if it doesn't change automatically"
echo "      Save the User record"
echo ""
echo "Add in Override for tokens"
echo "  Go to Setup > Custom Settings > Merc Token Override > Create new Record, check Override box, assign to System Admin Profile to bypass Authentication"
echo ""
echo "Add in Site Key for ReCaptcha"
echo "  Go to Setup > Custom Settings > merc Captcha Setting > Create new Record, enter site key '6Lftt6QUAAAAAOQbn6rTwmj6XpQqAGk0yl_EKxPs'"
echo ""
echo "Set up Encryption (if necessary)"
echo "  Go to Dev Console > Run Anonymous Apex > merc_EncryptionHandler.generateNewEncryptionKey();"
