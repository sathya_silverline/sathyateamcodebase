# Getting Started

1. [Install SFDX](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_install_cli.htm#sfdx_setup_install_cli)
2. Log into production and sets it as the default DevHub

```
sfdx force:auth:web:login -d -a INTLProd
```

[Command Reference](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference.htm)

* Commands followed by either -h or --help will give you detailed information on their use

## Visual Studio Code

Salesforce has created a handful of packages for VSCode that will handle a lot of work for you, all of their commands are prefaced with SFDX so you can easily open up the command palette and find all the standard commands for SFDX just by typing SFDX.

# SFDX Development Process for Mercury

1. Create a feature branch corresponding to the ticket you are working on
2. Do your development and testing in a scratch org. Be sure you are developing in the mercury folder of the repo, the main folder is for any dependencies we need to bring over from INTL's existing Org. Mercury will be configured to automatically pull in anything from main and install it when mercury is installed
3. Once you feel the ticket is complete and all tests are passing for the project, create a pull request to merge your branch into master
4. Once the pull request goes through, the CI on master will install the mercury app into mercurydev for Silverline QA
5. Once it passes Silverline QA, it will get installed to mercuryqa for INTL QA

# Scratch.sh

## Running the Script

Be in the intl-mercury directory and run the following command, replace ALIAS which what you wish to call your scratch org:

```
./scratch.sh -a ALIAS
```
## Script Flow

The scratch.sh script is built to spin up a scratch org from the mercury project, run all tests and lint the lightning components. The flow of the script is as follows: (Note $ marks a variable passed in or found through the script)

```
cd mercury
```

Create a scratch org with the provided alias

```
sfdx force:org:create -s -f ./config/project-scratch-def.json -a $ALIAS
```

* -s sets this Org as the default username, you technically do not need to reference the Alias anymore unless you set a different scratch org as the default

Build a Community

```
cd ../community
sfdx force:source:push -u $ALIAS
cd ../mercury
```

Push local changes to the scratch org

```
sfdx force:source:push -u $ALIAS
```

Checks for data to import, iterates through the data folder and loads in any .json data files

```
sfdx force:data:tree:import --sobjecttreefiles $file
```

Open the scratch org

```
sfdx force:org:open -u $ALIAS
```

Running all tests

```
sfdx force:apex:test:run -u $ALIAS --wait 10
```

Lint Lightning

```
sfdx force:lightning:lint ./force-app/main/default/aura/
```

## Manual steps for Community

1. Go to Setup > Feature Settings > Communities > All Communities
    * For Mercury community, open Builder
    * Click Publish
2. In Builder, go to Settings > General
    * Click the link for Mercury Profile under the Guest User Profile heading
    * Click Edit on the Default Community related list (bottom of page)
    * Change Community dropdown to Mercury and Save

3. Complete setup for sample Community user
    * Go to Setup > Roles
    ** Create a new top-level role (role name does not matter)
    ** Assign the user record "User User" to the new role
    * Go to Contacts and open the John Doe record
    ** Click the Enable Customer User button
    *** Change User License to "Customer Community Plus Login"
    *** Change Profile to "Customer Community Plus Login User" if it doesn't change automatically
    *** Save the User record

4. Add In Override for SecureAuth
    * Go to Setup > Custom Settings > Merc Token Override > Create new Record, check Override box, assign to System Admin Profile to bypass Authentication

5. Set Encryption Key (If necessary)
    * Go to Dev Console > Run Anonymous Apex > merc_Encryptionhandler.generateNewEncryptionKey();
