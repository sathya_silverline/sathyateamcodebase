**Community Files Upload Project**  
The Silverline asset lightning component regarding file upload functionality embeded in the datatable.  

**Generic and extendable snippets**  
The generic lightning component which are having generic javascript functions as below:  
1. merc_ProviderCommon: Lightning component, helper JS has below generic functions:  
a. showToast()  
b. serverAction()  
c. navigateToURL()  
d. getUrlParam()  
e. getObjectFromId()  
f. dateCompare()  
g. parseDate()  
h. getStoredCookieValue()  
i. saveValueToCookie()  
j. clearStoredCookie(); 
  
**Vlocity**  
The folder contains datapacks of various custom built functionalities.  
1. Accordion Datatable.  
2. Dynamic Datatable with various functionalities.  
3. Search by columns in datatable.  