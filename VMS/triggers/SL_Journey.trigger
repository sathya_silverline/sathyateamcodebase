trigger SL_Journey on HealthCloudGA__ProgramPatientAffiliation__c (after insert) {
	SL_Trigger.dispatchHandler(HealthCloudGA__ProgramPatientAffiliation__c.SObjectType,new SL_JourneyTriggerHandler());
}