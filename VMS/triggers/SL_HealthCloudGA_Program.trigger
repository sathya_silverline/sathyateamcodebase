trigger SL_HealthCloudGA_Program on HealthCloudGA__Program__c (before insert, before update) {
	SL_Trigger.dispatchHandler(HealthCloudGA__Program__c.SObjectType,new SL_ProgramTriggerHandler());
}