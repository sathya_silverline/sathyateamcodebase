public class SL_EventTriggerHandler extends SL_Trigger.baseHandler {
	// Before Insert
    public override void afterInsert(Map<Id, SObject> newEvents){
        System.debug('[INFO]: BEFORE INSERT - NEW-EVENTS: ' + newEvents);
        System.debug('[INFO]: BEFORE UPDATE - NEW-EVENTS: ' + newEvents);

        Map<Id, Event> newEventsMap = (Map<Id, Event>) newEvents;
        List<Event> newEventsList = new List<Event>();
        newEventsList.addAll(newEventsMap.values());
        validateEventType(newEventsList, newEventsMap);
    }

    // Before Update
    public override void afterUpdate(Map<Id, SObject> oldEvents, Map<Id, SObject> newEvents){
        System.debug('[INFO]: BEFORE UPDATE - OLD-EVENTS: ' + oldEvents);
        System.debug('[INFO]: BEFORE UPDATE - NEW-EVENTS: ' + newEvents);

        Map<Id, Event> newEventsMap = (Map<Id, Event>) newEvents;
        List<Event> newEventsList = new List<Event>();
        newEventsList.addAll(newEventsMap.values());
        validateEventType(newEventsList, newEventsMap);
    }

    public void validateEventType(List<Event> lstEvents, Map<Id, Event> newEventsMap){

        Integer errorCount = 0;
        String casePrefix = Case.getSObjectType().getDescribe().getKeyPrefix();
        Set<Id> setCases = new Set<Id>();
        Map<Id, Id> mapOfEventIdToCaseId = new Map<Id, Id>();
        for(Event objEvent: lstEvents){
            if(objEvent.WhatId != null){
                String strWhatId = String.valueOf(objEvent.WhatId);
                if(casePrefix == strWhatId.substring(0,3)){
                    mapOfEventIdToCaseId.put(objEvent.Id, objEvent.WhatId);
                    setCases.add(objEvent.WhatId);
                }
            }
        }

        List<Case> lstCases = new Lis<Case>();
        Map<Id, Case> mapOdIfToCaseRec = new Map<Id, Case>();
        if(!setCases.isEmpty()){
            lstCases = [SELECT Id, Type FROM Case WHERE Id IN :setCases];
            for(Case objCase : lstCases){
                mapOdIfToCaseRec.put(Id, objCase);
            }
        }

        for(Id objEventId: mapOfEventIdToCaseId.keySet()){
            if(newEventsMap.get(objEventId).Type != null){
                if(mapOdIfToCaseRec.get(mapOfEventIdToCaseId.get(objEventId)).Type == 'Telephonic' || mapOdIfToCaseRec.get(mapOfEventIdToCaseId.get(objEventId)).Type == 'Live' || newEventsMap.get(objEventId).Type == 'Telephonic' || newEventsMap.get(objEventId).Type == 'Live'){
                    if(mapOdIfToCaseRec.get(mapOfEventIdToCaseId.get(objEventId)).Type != newEventsMap.get(objEventId).Type){
                        errorCount++;
                    }
                }
            }
        }

        if(errorCount > 0){
            throw new Exception('Type must be same as the Engagement');
        }
    }
}