/**
 * Name         : SL_AccountTriggerHandler
 * Description  : It is the default handler class of SL_Account Trigger.
 * Created Date : 13th June, 2019
 * Modified Date: 13th June, 2019
 * Created By   : Amit Kumar
**/

public class SL_AccountTriggerHandler extends SL_Trigger.baseHandler {
    
    private Id OwnerId;
    private Id PatientRecordTypeId;
    private Id CaregiverRecordTypeId;
    private Id EducatorRecordTypeId;
    
    public SL_AccountTriggerHandler(){
        getPreRequisites(); // Get Pre-Requisites
    }
    
    // Before Insert
    public override void beforeInsert(List<SObject> newAccounts){
        System.debug('[INFO]: BEFORE INSERT - NEW-ACCOUNTS: ' + newAccounts);
        List<Account> lstAccounts = (List<Account>) newAccounts;

        executeAccountUpdates(lstAccounts);
    }

    // Before Update
    public override void beforeUpdate(Map<Id, SObject> oldAccounts, Map<Id, SObject> newAccounts){
        System.debug('[INFO]: BEFORE UPDATE - OLD-ACCOUNTS: ' + oldAccounts);
        System.debug('[INFO]: BEFORE UPDATE - NEW-ACCOUNTS: ' + newAccounts);

        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newAccounts;
        List<Account> newAccountsList = new List<Account>();
        newAccountsList.addAll(newAccountsMap.values());

        executeAccountUpdates(newAccountsList);
    }

    public void getPreRequisites(){
        PatientRecordTypeId = CaregiverRecordTypeId = EducatorRecordTypeId = null;
        // Get Record type Ids
        PatientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        CaregiverRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
        EducatorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        
        User vmsAdmin = [SELECT Id, Name FROM User WHERE Name = 'VMS Admin' LIMIT 1];
        OwnerId = vmsAdmin.Id;
    }

    // Helper to process the data and update accordingly
    public void executeAccountUpdates(List<Account> lstAccounts){
        Set<Id> setOfBrands = new Set<Id>();
        Set<Id> setOfPatients = new Set<Id>();
        Map<Id, String> mapOfBrandIdToBrandNames = new Map<Id, String>();
        Map<Id, String> mapOfPatientIdToBrandNames = new Map<Id, String>();
        Map<Id, Id> mapOfPatientIdToBrandId = new Map<Id, Id>();
        Set<Id> setOfPatientAccs = new Set<Id>();

        for(Account accObj : lstAccounts){
            if(accObj.Brand__c != null)
                setOfBrands.add(accObj.Brand__c);
            if(accObj.Patient__c != null)
                setOfPatients.add(accObj.Patient__c);
            
            if(accObj.RecordTypeId == PatientRecordTypeId){
                setOfPatientAccs.add(accObj.Id);
            }
        }

        if(!setOfPatients.isEmpty()){
            for(Account objPatient : [SELECT Id, Name, Brand__c FROM Account WHERE Id IN :setOfPatients]){
                if(objPatient.Brand__c != null){
                    mapOfPatientIdToBrandId.put(objPatient.Id, objPatient.Brand__c);
                    setOfBrands.add(objPatient.Brand__c);
                }
            }
        }

        if(!setOfBrands.isEmpty()){
            for(Account objBrand : [SELECT Id, Name FROM Account WHERE Id IN :setOfBrands]){
                mapOfBrandIdToBrandNames.put(objBrand.Id, objBrand.Name);
            }
        }

        for(Account accObj : lstAccounts){
            // Owner updated to VMS Admin for non-person accounts, Patient accounts
            if(!accObj.IsPersonAccount || accObj.RecordTypeId == PatientRecordTypeId){
                accObj.OwnerId = OwnerId;
            }

            // Brand_Name__c is updated for patient record type
            if(accObj.RecordTypeId == PatientRecordTypeId){
                if(accObj.Brand__c != null)
                    accObj.Brand_Name__c = mapOfBrandIdToBrandNames.get(accObj.Brand__c);
                mapOfPatientIdToBrandNames.put(accObj.Id, mapOfBrandIdToBrandNames.get(accObj.Brand__c));
            }

            // Brand_Name__c is updated for caregiver record type
            if(accObj.RecordTypeId == CaregiverRecordTypeId){
                if(accObj.Patient__c != null && mapOfBrandIdToBrandNames.get(mapOfPatientIdToBrandId.get(accObj.Patient__c)) != null)
                    accObj.Brand_Name__c = mapOfBrandIdToBrandNames.get(mapOfPatientIdToBrandId.get(accObj.Patient__c));
            }

            // Owner Id is updated to Educator_User__c for Educator record type
            if(accObj.RecordTypeId == EducatorRecordTypeId){
                if(accObj.Educator_User__c != null && accObj.OwnerId != accObj.Educator_User__c){
                    accObj.OwnerId = accObj.Educator_User__c;
                }
            }
        }
        updateCaregiversBrandForPatientAccs(mapOfPatientIdToBrandNames, setOfPatientAccs);
    }

    // Helper to update Caregivers with Patient__r.Brand__r.Name for Patient Accounts
    public void updateCaregiversBrandForPatientAccs(Map<Id, String> mapOfPatientIdToBrandNames, Set<Id> setOfPatientAccs){
        List<Account> lstCaregivers = new List<Account>();
        lstCaregivers = [SELECT Id, Patient__c, Brand__c FROM Account WHERE RecordTypeId = :CaregiverRecordTypeId AND Patient__c IN :setOfPatientAccs];

        List<Account> lstCaregiversToUpdate = new List<Account>();

        for(Account caregiverObj : lstCaregivers){
            caregiverObj.Brand_Name__c = mapOfPatientIdToBrandNames.get(caregiverObj.Patient__c);
            lstCaregiversToUpdate.add(caregiverObj);
        }
        update lstCaregiversToUpdate;
    }

    // Handled recursion which occurs during Caregivers update
    public override void beforeInsertRecursive(List<SObject> newListGeneric){}
    public override void beforeUpdateRecursive(Map<Id, SObject> oldMapGeneric, Map<Id, SObject> newMapGeneric){}
}