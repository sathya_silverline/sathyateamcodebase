/**
 * Name         : SL_CaseTriggerHandler
 * Description  : It is the default handler class of SL_Case Trigger.
 * Created Date : 17th June, 2019
 * Modified Date: 18th June, 2019
 * Created By   : Amit Kumar
**/

public class SL_CaseTriggerHandler extends SL_Trigger.baseHandler {

    // Before Insert
    public override void beforeInsert(List<SObject> newCases){
        System.debug('[INFO]: BEFORE INSERT - NEW-CASES: ' + newCases);
        List<Case> lstCases = (List<Case>) newCases;
        updateCases(lstCases);
    }

    // Before Update
    public override void beforeUpdate(Map<Id, SObject> oldCases, Map<Id, SObject> newCases){
        System.debug('[INFO]: BEFORE UPDATE - OLD-CASES: ' + oldCases);
        System.debug('[INFO]: BEFORE UPDATE - NEW-CASES: ' + newCases);

        Map<Id, Case> newCasesMap = (Map<Id, Case>) newCases;
        List<Case> newCasesList = new List<Case>();
        newCasesList.addAll(newCasesMap.values());
        updateCases(newCasesList);
    }

    public void updateCases(List<Case> lstCases){

        Set<Id> setOfJourneys = new Set<Id>();
        Map<Id, Id> mapOfCaseIdToJourneyId = new Map<Id, Id>();
        Map<Id, HealthCloudGA__ProgramPatientAffiliation__c> mapOfJourneyIdToJourneyRec = new Map<Id, HealthCloudGA__ProgramPatientAffiliation__c>();

        for(Case objCase : lstCases){
            if(objCase.Journey__c != null){
                setOfJourneys.add(objCase.Journey__c);
                mapOfCaseIdToJourneyId.put(objCase.Id, objCase.Journey__c);
            }
        }

        List<HealthCloudGA__ProgramPatientAffiliation__c> lstJourneys = new List<HealthCloudGA__ProgramPatientAffiliation__c>();
        if(!setOfJourneys.isEmpty()){
            lstJourneys = [SELECT Id, RecordType.Name, HealthCloudGA__Program__r.Brand_Name__c, Patient_VMS__c, Patient_VMS__r.PersonContactId, Provider_VMS__c FROM HealthCloudGA__ProgramPatientAffiliation__c WHERE Id IN :setOfJourneys];
        }

        for(HealthCloudGA__ProgramPatientAffiliation__c objJourney : lstJourneys){
            mapOfJourneyIdToJourneyRec.put(objJourney.Id, objJourney);
        }

        for(Case objCase : lstCases){
            if(mapOfCaseIdToJourneyId.containsKey(objCase.Id)){

                // Set Brand_Name__c from the related Program record
                String strBrandName = mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).HealthCloudGA__Program__r.Brand_Name__c;
                if(String.isNotBlank(strBrandName)){
                    objCase.Brand_Name__c = strBrandName;
                }

                if(mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).RecordType.Name == 'Patient Journey'){
                    if(mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).Patient_VMS__c != null){
                        objCase.ContactId = mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).Patient_VMS__r.PersonContactId; // Set Contact Id with related Patient_VMS__c
                    }
                }
                else if(mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).RecordType.Name == 'Provider Journey'){
                    if(mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).Provider_VMS__c != null){
                        objCase.ContactId = mapOfJourneyIdToJourneyRec.get(mapOfCaseIdToJourneyId.get(objCase.Id)).Provider_VMS__c; // Set Contact Id with related Provider_VMS__c
                    }
                }
            }
        }
    }
}