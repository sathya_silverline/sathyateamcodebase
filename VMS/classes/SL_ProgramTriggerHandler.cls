/**
 * Name         : SL_ProgramTriggerHandler
 * Description  : It is the default handler class of SL_HealthCloudGA_Program Trigger.
 * Created Date : 18th June, 2019
 * Modified Date: 18th June, 2019
 * Created By   : Amit Kumar
**/

public class SL_ProgramTriggerHandler extends SL_Trigger.baseHandler{
	
    // Before Insert
    public override void beforeInsert(List<SObject> newPrograms){
        System.debug('[INFO]: BEFORE INSERT - NEW-PROGRAMS: ' + newPrograms);
        List<HealthCloudGA__Program__c> lstPrograms = (List<HealthCloudGA__Program__c>) newPrograms;
        updatePrograms(lstPrograms);
    }

    // Before Update
    public override void beforeUpdate(Map<Id, SObject> oldPrograms, Map<Id, SObject> newPrograms){
        System.debug('[INFO]: BEFORE UPDATE - OLD-PROGRAMS: ' + oldPrograms);
        System.debug('[INFO]: BEFORE UPDATE - NEW-PROGRAMS: ' + newPrograms);

        Map<Id, HealthCloudGA__Program__c> newProgramsMap = (Map<Id, HealthCloudGA__Program__c>) newPrograms;
        List<HealthCloudGA__Program__c> newProgramsList = new List<HealthCloudGA__Program__c>();
        newProgramsList.addAll(newProgramsMap.values());
        updatePrograms(newProgramsList);
    }
    
    public void updatePrograms(List<HealthCloudGA__Program__c> lstPrograms){
        
        Set<Id> setBrandIds = new Set<Id>();
        Map<Id, Id> mapOfProgIdToBrandId = new Map<Id, Id>();
        Map<Id, Account> mapOfBrandIdToBrandObj = new Map<Id, Account>();
        for(HealthCloudGA__Program__c progObj : lstPrograms){
            if(progObj.Brand__c != null){
                setBrandIds.add(progObj.Brand__c);
                mapOfProgIdToBrandId.put(progObj.Id, progObj.Brand__c);
            }
        }
        
        List<Account> lstBrands = new List<Account>();
        if(!setBrandIds.isEmpty()){
            lstBrands = [SELECT Id, Name FROM Account WHERE Id IN :setBrandIds];
            for(Account objBrand : lstBrands){
                mapOfBrandIdToBrandObj.put(objBrand.Id, objBrand);
            }
        }
        
        for(HealthCloudGA__Program__c progObj : lstPrograms){
            if(mapOfProgIdToBrandId.containsKey(progObj.Id)){
                progObj.Brand_Name__c = mapOfBrandIdToBrandObj.get(mapOfProgIdToBrandId.get(progObj.Id)).Name;
            }
        }
    }
}