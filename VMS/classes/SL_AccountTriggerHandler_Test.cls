/**
 * Name         : SL_AccountTriggerHandler_Test
 * Description  : Test class for SL_AccountTriggerHandler.cls
 * Created Date : 17th June, 2019
 * Modified Date: 17th June, 2019
 * Created By   : Amit Kumar
**/

@isTest
public class SL_AccountTriggerHandler_Test {
	private static Id ClientRecordTypeId;
    private static Id BrandRecordTypeId;
    private static Id PatientRecordTypeId;
    private static Id CaregiverRecordTypeId;
    private static Id EducatorRecordTypeId;
    
    public static void testDataSetup(){
        // Non-person account recordtype
        ClientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        BrandRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Brand').getRecordTypeId();
        
        // Person account recordtype
        PatientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        CaregiverRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
        EducatorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
    }
    
    @isTest
    public static void testNonPersonAccounts(){
        testDataSetup();
        List<Account> lstNonPersonAccounts = (List<Account>)SL_TestDataFactory.createSObjectList(new Account(RecordTypeId = ClientRecordTypeId),100,true);
        List<Account> lstClientAccounts = [SELECT Id, Name, Owner.Name FROM Account WHERE isPersonAccount = false AND RecordTypeId = :ClientRecordTypeId];
        for(Account clientAcc : lstClientAccounts){
            Boolean assertResult = false;
            if(clientAcc.Owner.Name == 'VMS Admin'){
                assertResult = true;
            }
            SL_TestDataFactory.softAssert(assertResult, 'Owner for Non-Person Accounts is ' + clientAcc.Owner.Name);
        }
        
        SL_TestDataFactory.hardAssertAllResults();
    }
    
    @isTest
    public static void testPatientAccounts(){
        testDataSetup();
        Account objBrand = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand'),true);
        List<Account> lstPatients = new List<Account>();
        for(Integer i = 0; i < 100; i++){
            Account objPatient = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient ' + i, LastName = 'Patient ' + i, Brand__c = objBrand.Id);
            lstPatients.add(objPatient);
        }
        insert lstPatients;
        
        List<Account> lstBrand = [SELECT Id, Name FROM Account WHERE RecordTypeId = :BrandRecordTypeId LIMIT 1];
        List<Account> lstPatientsQueried = [SELECT Id, Name, Owner.Name, Brand__c, Brand_Name__c FROM Account WHERE RecordTypeId = :PatientRecordTypeId];
        for(Account patientAcc : lstPatientsQueried){
            Boolean assertOwnerResult = false;
            if(patientAcc.Owner.Name == 'VMS Admin'){
                assertOwnerResult = true;
            }
            SL_TestDataFactory.softAssert(assertOwnerResult, 'Owner for Patient is ' + patientAcc.Owner.Name);
            
            Boolean assertBrandNameResult = false;
            if(patientAcc.Brand_Name__c == lstBrand.get(0).Name){
                assertBrandNameResult = true;
            }
            SL_TestDataFactory.softAssert(assertBrandNameResult, 'Brand_Name__c for Patient is ' + patientAcc.Brand_Name__c);
        }
        SL_TestDataFactory.hardAssertAllResults();
    }
    
    @isTest
    public static void testcaregiverAccounts(){
        testDataSetup();
        Account objBrand = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand'),true);
        Account objPatient = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient', LastName = 'Patient', Brand__c = objBrand.Id);
        insert objPatient;
        List<Account> lstCGs = new List<Account>();
        for(Integer i = 0; i < 100; i++){
            Account objCG = new Account(RecordTypeId = CaregiverRecordTypeId, FirstName = 'Test Patient ' + i, LastName = 'Patient ' + i, Patient__c = objPatient.Id);
            lstCGs.add(objCG);
        }
        insert lstCGs;
        
        List<Account> lstPatient = [SELECT Id, Name, Owner.Name, Brand__r.Name, Brand_Name__c FROM Account WHERE RecordTypeId = :PatientRecordTypeId LIMIT 1];
        List<Account> lstCaregivers = [SELECT Id, Name, Owner.Name, Patient__c, Brand_Name__c FROM Account WHERE RecordTypeId = :CaregiverRecordTypeId];
        
        for(Account caregiverAcc : lstCaregivers){
            
            Boolean assertBrandNameResult = false;
            if(caregiverAcc.Brand_Name__c == lstPatient.get(0).Brand__r.Name){
                assertBrandNameResult = true;
            }
            SL_TestDataFactory.softAssert(assertBrandNameResult, 'Brand_Name__c for Caregiver is ' + caregiverAcc.Brand_Name__c);
        }
        SL_TestDataFactory.hardAssertAllResults();
    }
    
    @isTest
    public static void testPatientAccountsUpdateCase(){
        testDataSetup();
        Account objBrandOne = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand 1'),true);
        Account objBrandTwo = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand 2'),true);
        Account objPatient = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient', LastName = 'Patient', Brand__c = objBrandOne.Id);
        insert objPatient;
        List<Account> lstCGs = new List<Account>();
        for(Integer i = 0; i < 100; i++){
            Account objCG = new Account(RecordTypeId = CaregiverRecordTypeId, FirstName = 'Test Patient ' + i, LastName = 'Patient ' + i, Patient__c = objPatient.Id);
            lstCGs.add(objCG);
        }
        insert lstCGs;
        
        Account objPatientQr = [SELECT Id, Brand__c FROM Account WHERE RecordTypeId = :PatientRecordTypeId LIMIT 1];
        objPatientQr.Brand__c = objBrandTwo.Id;
        update objPatientQr;
        
        Account objPatientQueried = [SELECT Id, Name, Brand__c, Brand__r.Name FROM Account WHERE RecordTypeId = :PatientRecordTypeId LIMIT 1];
        List<Account> lstCaregivers = [SELECT Id, Name, Owner.Name, Patient__c, Brand_Name__c FROM Account WHERE RecordTypeId = :CaregiverRecordTypeId];
        
        for(Account caregiverAcc : lstCaregivers){
            
            Boolean assertBrandNameResult = false;
            if(caregiverAcc.Brand_Name__c == objPatientQueried.Brand__r.Name){
                assertBrandNameResult = true;
            }
            SL_TestDataFactory.softAssert(assertBrandNameResult, 'Brand_Name__c for Caregiver is ' + caregiverAcc.Brand_Name__c);
        }
        
        SL_TestDataFactory.hardAssertAllResults();
    }
}