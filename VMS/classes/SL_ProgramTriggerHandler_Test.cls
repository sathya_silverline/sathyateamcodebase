/**
 * Name         : SL_ProgramTriggerHandler_Test
 * Description  : Test class for SL_ProgramTriggerHandler.cls
 * Created Date : 18th June, 2019
 * Modified Date: 18th June, 2019
 * Created By   : Amit Kumar
**/

@isTest
public class SL_ProgramTriggerHandler_Test {
	@isTest
    public static void testProgramUpdates(){
        Id BrandRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Brand').getRecordTypeId();
        Id PatientProgramRecordTypeId = Schema.SObjectType.HealthCloudGA__Program__c.getRecordTypeInfosByName().get('Patient Program').getRecordTypeId();
        
        List<Account> lstBrands = (List<Account>)SL_TestDataFactory.createSObjectList(new Account(RecordTypeId = BrandRecordTypeId),2,true);
        List<HealthCloudGA__Program__c> lstPrograms = new List<HealthCloudGA__Program__c>();
        for(Integer i = 0; i < 100; i++){
            HealthCloudGA__Program__c objProgram = new HealthCloudGA__Program__c(RecordTypeId = PatientProgramRecordTypeId, Name = 'Test Program ' + i, Brand__c = lstBrands.get(0).Id, HealthCloudGA__StartDate__c = Date.today(), HealthCloudGA__EndDate__c = Date.today().addDays(1));
            lstPrograms.add(objProgram);
        }
        insert lstPrograms;
        
        List<HealthCloudGA__Program__c> lstProgsQueried = [SELECT Id, Brand__r.Name, Brand_Name__c FROM HealthCloudGA__Program__c];
        for(HealthCloudGA__Program__c objProg : lstProgsQueried) {
            SL_TestDataFactory.softAssertEquals(objProg.Brand__r.Name, objProg.Brand_Name__c);
        }
        
        List<HealthCloudGA__Program__c> lstProgsToUpdate = new List<HealthCloudGA__Program__c>();
        
        for(HealthCloudGA__Program__c objProg : lstPrograms){
            objProg.Brand__c = lstBrands.get(1).Id;
            lstProgsToUpdate.add(objProg);
        }
        update lstProgsToUpdate;
        
        List<HealthCloudGA__Program__c> lstProgsQueried1 = [SELECT Id, Brand__r.Name, Brand_Name__c FROM HealthCloudGA__Program__c];
        for(HealthCloudGA__Program__c objProg : lstProgsQueried1) {
            SL_TestDataFactory.softAssertEquals(objProg.Brand__r.Name, objProg.Brand_Name__c);
        }
        
        SL_TestDataFactory.hardAssertAllResults();
    }
}