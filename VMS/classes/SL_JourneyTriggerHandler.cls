/**
 * Name         : SL_JourneyTriggerHandler
 * Description  : It is the default handler class of SL_Journey Trigger.
 * Created Date : 19th June, 2019
 * Modified Date: 20th June, 2019
 * Created By   : Amit Kumar
**/

public class SL_JourneyTriggerHandler extends SL_Trigger.baseHandler {
	
    // After Insert
    public override void afterInsert(Map<Id, SObject> newJourneys){
        System.debug('[INFO]: AFTER INSERT - NEW-JOURNEYS: ' + newJourneys);

        Map<Id, HealthCloudGA__ProgramPatientAffiliation__c> newJourneysMap = (Map<Id, HealthCloudGA__ProgramPatientAffiliation__c>) newJourneys;
        List<HealthCloudGA__ProgramPatientAffiliation__c> newJourneysList = new List<HealthCloudGA__ProgramPatientAffiliation__c>();
        newJourneysList.addAll(newJourneysMap.values());

        Map<Id, Id> mapOfJourneyIdToProgramId = new Map<Id, Id>();
        Set<Id> setOfPrograms = new Set<Id>();
        for(HealthCloudGA__ProgramPatientAffiliation__c objJourney : newJourneysList){
            mapOfJourneyIdToProgramId.put(objJourney.Id, objJourney.HealthCloudGA__Program__c);
            setOfPrograms.add(objJourney.HealthCloudGA__Program__c);
        }

        List<HealthCloudGA__Program__c> lstPrograms = [SELECT Id, Name, (SELECT Id, Name, Attribute_Details__c, Required__c, Program_Stage__c, Program_Stage__r.Name FROM Program_Attributes__r), (SELECT Id, Name, Program_Stage__c, Program_Stage__r.Name, Use__c, Material_Details__c FROM Program_Materials__r), (SELECT Id, Name, Order__c, Program_Stage__c, Program_Stage__r.Name FROM Program_Topics__r) FROM HealthCloudGA__Program__c WHERE Id IN :setOfPrograms];
        
        Map<Id, List<Program_Attribute__c>> mapOfProgramIdToAttributes = new Map<Id, List<Program_Attribute__c>>();
        Map<Id, List<Program_Material__c>> mapOfProgramIdToMaterials = new Map<Id, List<Program_Material__c>>();
        Map<Id, List<Program_Topic__c>> mapOfProgramIdToTopics = new Map<Id, List<Program_Topic__c>>();

        for(HealthCloudGA__Program__c objProg : lstPrograms){
            if(!objProg.Program_Attributes__r.isEmpty())
                mapOfProgramIdToAttributes.put(objProg.Id, objProg.Program_Attributes__r);
            if(!objProg.Program_Materials__r.isEmpty())
                mapOfProgramIdToMaterials.put(objProg.Id, objProg.Program_Materials__r);
            if(!objProg.Program_Topics__r.isEmpty())
                mapOfProgramIdToTopics.put(objProg.Id, objProg.Program_Topics__r);
        }

        createJourneyAttributes(mapOfJourneyIdToProgramId, mapOfProgramIdToAttributes, newJourneysMap);
        createJourneyMaterials(mapOfJourneyIdToProgramId, mapOfProgramIdToMaterials, newJourneysMap);
        createJourneyTopics(mapOfJourneyIdToProgramId, mapOfProgramIdToTopics, newJourneysMap);
    }

    public void createJourneyAttributes(Map<Id, Id> mapOfJourneyIdToProgramId, Map<Id, List<Program_Attribute__c>> mapOfProgramIdToAttributes, Map<Id, HealthCloudGA__ProgramPatientAffiliation__c> newJourneysMap){
        
        List<Journey_Attribute__c> lstJourneyAttrs = new List<Journey_Attribute__c>();
        for(Id journeyId : mapOfJourneyIdToProgramId.keySet()){
            if(mapOfProgramIdToAttributes.containsKey(mapOfJourneyIdToProgramId.get(journeyId))){
                for(Program_Attribute__c objProgAttr : mapOfProgramIdToAttributes.get(mapOfJourneyIdToProgramId.get(journeyId))){
                    Journey_Attribute__c objJourneyAttr = new Journey_Attribute__c(Journey__c = journeyId, Name = objProgAttr.Name, Attribute_Details__c = objProgAttr.Attribute_Details__c, Required__c = objProgAttr.Required__c, Stage__c = objProgAttr.Program_Stage__r.Name);
                    lstJourneyAttrs.add(objJourneyAttr);
                }
            }
        }
        insert lstJourneyAttrs;
        System.debug('lstJourneyAttrs>>>>' + lstJourneyAttrs);
    }

    public void createJourneyMaterials(Map<Id, Id> mapOfJourneyIdToProgramId, Map<Id, List<Program_Material__c>> mapOfProgramIdToMaterials, Map<Id, HealthCloudGA__ProgramPatientAffiliation__c> newJourneysMap){
        
        List<Journey_Material__c> lstJourneyMtrs = new List<Journey_Material__c>();
        for(Id journeyId : mapOfJourneyIdToProgramId.keySet()){
            if(mapOfProgramIdToMaterials.containsKey(mapOfJourneyIdToProgramId.get(journeyId))){
                for(Program_Material__c objProgMtr : mapOfProgramIdToMaterials.get(mapOfJourneyIdToProgramId.get(journeyId))){
                    Journey_Material__c objJourneyMtr = new Journey_Material__c(Journey__c = journeyId, Name = objProgMtr.Name, Details__c = objProgMtr.Material_Details__c, Stage__c = objProgMtr.Program_Stage__r.Name, Use__c = objProgMtr.Use__c);
                    lstJourneyMtrs.add(objJourneyMtr);
                }
            }
        }
        insert lstJourneyMtrs;
        System.debug('lstJourneyMtrs>>>>' + lstJourneyMtrs);
    }

    public void createJourneyTopics(Map<Id, Id> mapOfJourneyIdToProgramId, Map<Id, List<Program_Topic__c>> mapOfProgramIdToTopics, Map<Id, HealthCloudGA__ProgramPatientAffiliation__c> newJourneysMap){
        
        List<Journey_Topic__c> lstJourneyTopics = new List<Journey_Topic__c>();
        for(Id journeyId : mapOfJourneyIdToProgramId.keySet()){
            if(mapOfProgramIdToTopics.containsKey(mapOfJourneyIdToProgramId.get(journeyId))){
                for(Program_Topic__c objProgTopic : mapOfProgramIdToTopics.get(mapOfJourneyIdToProgramId.get(journeyId))){
                    Journey_Topic__c objJourneyTopic = new Journey_Topic__c(Journey__c = journeyId, Name = objProgTopic.Name, Order__c = objProgTopic.Order__c, Stage__c = objProgTopic.Program_Stage__r.Name);
                    lstJourneyTopics.add(objJourneyTopic);
                }
            }
        }
        insert lstJourneyTopics;
        System.debug('lstJourneyTopics>>>>' + lstJourneyTopics);
    }
}