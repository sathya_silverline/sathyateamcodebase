/**
 * Name         : SL_ContactTriggerHandler
 * Description  : It is the default handler class of SL_Contact Trigger.
 * Created Date : 17th June, 2019
 * Modified Date: 17th June, 2019
 * Created By   : Amit Kumar
**/

public class SL_ContactTriggerHandler extends SL_Trigger.baseHandler {
	
    private Id OwnerId;

    public SL_ContactTriggerHandler(){
        getPreRequisites(); // Get Pre-Requisites
    }
    // Before Insert
    public override void beforeInsert(List<SObject> newContacts){
        System.debug('[INFO]: BEFORE INSERT - NEW-CONTACTS: ' + newContacts);
        List<Contact> lstContacts = (List<Contact>) newContacts;

        executeContactUpdates(lstContacts);
    }

    // Before Update
    public override void beforeUpdate(Map<Id, SObject> oldContacts, Map<Id, SObject> newContacts){
        System.debug('[INFO]: BEFORE UPDATE - OLD-CONTACTS: ' + oldContacts);
        System.debug('[INFO]: BEFORE UPDATE - NEW-CONTACTS: ' + newContacts);

        Map<Id, Contact> newContactsMap = (Map<Id, Contact>) newContacts;
        List<Contact> newContactsList = new List<Contact>();
        newContactsList.addAll(newContactsMap.values());

        executeContactUpdates(newContactsList);
    }

    public void getPreRequisites(){
        User vmsAdmin = [SELECT Id, Name FROM User WHERE Name = 'VMS Admin' LIMIT 1];
        OwnerId = vmsAdmin.Id;
    }

    public void executeContactUpdates(List<Contact> lstContacts){
        for(Contact conObj : lstContacts){
            // Owner updated to VMS Admin for non-person contact
            if(!conObj.IsPersonAccount){
                conObj.OwnerId = OwnerId;
            }
        }
    }
}