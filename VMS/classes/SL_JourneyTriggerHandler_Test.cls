/**
 * Name         : SL_JourneyTriggerHandler_Test
 * Description  : Test class for SL_JourneyTriggerHandler.cls
 * Created Date : 20th June, 2019
 * Modified Date: 20th June, 2019
 * Created By   : Amit Kumar
**/

@isTest
public class SL_JourneyTriggerHandler_Test {
	
    @isTest
    public static void testJourneyTrigger(){
        Id BrandRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Brand').getRecordTypeId();
        Id PatientProgramRecordTypeId = Schema.SObjectType.HealthCloudGA__Program__c.getRecordTypeInfosByName().get('Patient Program').getRecordTypeId();
        Id PatientJourneyRecordTypeId = Schema.SObjectType.HealthCloudGA__ProgramPatientAffiliation__c.getRecordTypeInfosByName().get('Patient Journey').getRecordTypeId();
        Id PatientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

        Account objBrand = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand'),true);
        Account objPatient = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient', LastName = 'Patient', Brand__c = objBrand.Id);
        insert objPatient;

        HealthCloudGA__Program__c objProgram = new HealthCloudGA__Program__c(RecordTypeId = PatientProgramRecordTypeId, Name = 'Test Program', Brand__c = objBrand.Id, HealthCloudGA__StartDate__c = Date.today(), HealthCloudGA__EndDate__c = Date.today().addDays(1));
        insert objProgram;

        Program_Stage__c objPStage = new Program_Stage__c(Name = 'Test Stage', Program__c = objProgram.Id);
        insert objPStage;

        List<Program_Attribute__c> lstPAttrs = (List<Program_Attribute__c>)SL_TestDataFactory.createSObjectList(new Program_Attribute__c(Name = 'Test Prog Attribute', Program_Stage__c = objPStage.Id, Program__c = objProgram.Id), 100, true);
        List<Program_Material__c> lstPMtrs = (List<Program_Material__c>)SL_TestDataFactory.createSObjectList(new Program_Material__c(Name = 'Test Prog Material', Program_Stage__c = objPStage.Id, Program__c = objProgram.Id), 100, true);
        List<Program_Topic__c> lstPTopics = (List<Program_Topic__c>)SL_TestDataFactory.createSObjectList(new Program_Topic__c(Name = 'Test Prog Topic', Program_Stage__c = objPStage.Id, Program__c = objProgram.Id), 100, true);

        HealthCloudGA__ProgramPatientAffiliation__c objJourney = new HealthCloudGA__ProgramPatientAffiliation__c(Patient_VMS__c = objPatient.Id, HealthCloudGA__Program__c = objProgram.Id, Brand__c = objBrand.Id);
        insert objJourney;

        List<Journey_Attribute__c> lstJAttrs = [SELECT Id, Name FROM Journey_Attribute__c];
        SL_TestDataFactory.softAssertEquals(100, lstJAttrs.size());
        List<Journey_Material__c> lstJMtrs = [SELECT Id, Name FROM Journey_Material__c];
        SL_TestDataFactory.softAssertEquals(100, lstJMtrs.size());
        List<Journey_Topic__c> lstJTopics = [SELECT Id, Name FROM Journey_Topic__c];
        SL_TestDataFactory.softAssertEquals(100, lstJTopics.size());

        SL_TestDataFactory.hardAssertAllResults();
    }
}