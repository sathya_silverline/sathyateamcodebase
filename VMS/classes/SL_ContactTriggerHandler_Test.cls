/**
 * Name         : SL_ContactTriggerHandler_Test
 * Description  : Test class for SL_ContactTriggerHandler.cls
 * Created Date : 17th June, 2019
 * Modified Date: 17th June, 2019
 * Created By   : Amit Kumar
**/

@isTest
public class SL_ContactTriggerHandler_Test {
	@isTest
    public static void testNonPersonContacts(){
        Id StaffRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
        Id ClientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account objNonPersonAccount = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = ClientRecordTypeId),true);
        List<Contact> lstNonPersonContacts = (List<Contact>)SL_TestDataFactory.createSObjectList(new Contact(RecordTypeId = StaffRecordTypeId, AccountId = objNonPersonAccount.Id),100,true);

        List<Contact> lstStaffContactsNew = new List<Contact>();

        List<Contact> lstStaffContacts = [SELECT Id, FirstName, Owner.Name FROM Contact WHERE isPersonAccount = false AND RecordTypeId = :StaffRecordTypeId];
        for(Contact staffContact : lstStaffContacts){
            Boolean assertResult = false;
            if(staffContact.Owner.Name == 'VMS Admin'){
                assertResult = true;
            }
            SL_TestDataFactory.softAssert(assertResult, 'Owner for Non-Person Contact is ' + staffContact.Owner.Name);

            staffContact.FirstName = 'Universal Contact';
            lstStaffContactsNew.add(staffContact);
        }
        update lstStaffContactsNew;
        
        SL_TestDataFactory.hardAssertAllResults();
    }
}