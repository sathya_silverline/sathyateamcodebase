/**
 * Name         : SL_CaseTriggerHandler_Test
 * Description  : Test class for SL_CaseTriggerHandler.cls
 * Created Date : 18th June, 2019
 * Modified Date: 18th June, 2019
 * Created By   : Amit Kumar
**/

@isTest
public class SL_CaseTriggerHandler_Test {
    private static Id BrandRecordTypeId;
    private static Id PatientRecordTypeId;
    private static Id PatientProgramRecordTypeId;
    private static Id PatientJourneyRecordTypeId;
    private static Id ProviderJourneyRecordTypeId;
    
    public static void testDataSetup(){
        BrandRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Brand').getRecordTypeId();
        PatientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

        PatientProgramRecordTypeId = Schema.SObjectType.HealthCloudGA__Program__c.getRecordTypeInfosByName().get('Patient Program').getRecordTypeId();

        PatientJourneyRecordTypeId = Schema.SObjectType.HealthCloudGA__ProgramPatientAffiliation__c.getRecordTypeInfosByName().get('Patient Journey').getRecordTypeId();
        ProviderJourneyRecordTypeId = Schema.SObjectType.HealthCloudGA__ProgramPatientAffiliation__c.getRecordTypeInfosByName().get('Provider Journey').getRecordTypeId();
    }

    @isTest
    public static void testEngagementUpdates(){
        testDataSetup();
        Account objBrand = (Account)SL_TestDataFactory.createSObject(new Account(RecordTypeId = BrandRecordTypeId, Name = 'Test Brand'),true);
        Account objPatient = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient 1', LastName = 'Patient 1', Brand__c = objBrand.Id);
        insert objPatient;
        Account objPatient_1 = new Account(RecordTypeId = PatientRecordTypeId, FirstName = 'Test Patient 2', LastName = 'Patient 2', Brand__c = objBrand.Id);
        insert objPatient_1;

        HealthCloudGA__Program__c objProgram = new HealthCloudGA__Program__c(RecordTypeId = PatientProgramRecordTypeId, Name = 'Test Program 1', Brand__c = objBrand.Id, HealthCloudGA__StartDate__c = Date.today(), HealthCloudGA__EndDate__c = Date.today().addDays(1));
        insert objProgram;

        HealthCloudGA__ProgramPatientAffiliation__c objJourneyPatient = new HealthCloudGA__ProgramPatientAffiliation__c(RecordTypeId = PatientJourneyRecordTypeId, Patient_VMS__c = objPatient.Id, HealthCloudGA__Program__c = objProgram.Id);
        insert objJourneyPatient;

        List<Account> lstPatientsQueried = [SELECT Id, Name, PersonContactId FROM Account WHERE RecordTypeId = :PatientRecordTypeId AND Id = :objPatient_1.Id];
        HealthCloudGA__ProgramPatientAffiliation__c objJourneyProvider = new HealthCloudGA__ProgramPatientAffiliation__c(RecordTypeId = ProviderJourneyRecordTypeId, Provider_VMS__c = lstPatientsQueried.get(0).PersonContactId, HealthCloudGA__Program__c = objProgram.Id);
        insert objJourneyProvider;

        Case objEngagement = new Case(Journey__c = objJourneyPatient.Id, Origin = 'Email');
        insert objEngagement;

        Case objCaseQueried = [SELECT Id, Brand_Name__c, ContactId FROM Case WHERE Id = :objEngagement.Id];
        Account objPatientsQueried1 = [SELECT Id, Name, PersonContactId FROM Account WHERE RecordTypeId = :PatientRecordTypeId AND Id = :objPatient.Id];

        SL_TestDataFactory.softAssertEquals(objCaseQueried.Brand_Name__c, 'Test Brand');
        SL_TestDataFactory.softAssertEquals(objCaseQueried.ContactId, objPatientsQueried1.PersonContactId);

        Case objEngagement1 = new Case(Journey__c = objJourneyProvider.Id, Origin = 'Email');
        insert objEngagement1;

        objEngagement1.Origin = 'Web';
        update objEngagement1;

        Case objCaseQueried1 = [SELECT Id, Brand_Name__c, ContactId FROM Case WHERE Id = :objEngagement1.Id];

        SL_TestDataFactory.softAssertEquals(objCaseQueried1.ContactId, lstPatientsQueried.get(0).PersonContactId);

        SL_TestDataFactory.hardAssertAllResults();

    }
}