({
	doInit: function (component, event, helper) {
		helper.loadData(component);
	},
	handleSearchInput: function (component, event, helper) {
		var oldSearchText = event.getParam('oldValue');
		var searchText = event.getParam('value');
		setTimeout(function () {
			if (oldSearchText !== searchText && searchText.length >= 3) {
				helper.loadData(component);
			}
		}, 300);
		if (searchText.length == 0) helper.loadData(component);
	},
	navigateToRecord: function (component, event) {
		var recordId = event.getSource().get('v.id');
		var navEvent = $A.get("e.force:navigateToSObject");
		navEvent.setParams({
			"recordId": recordId,
			"slideDevName": "related"
		});
		navEvent.fire();
	},
	previewRecord: function (component, event) {
		var Id = event.currentTarget.id;
		$A.get('e.lightning:openFiles').fire({
			recordIds: [Id]
		});
	}
})