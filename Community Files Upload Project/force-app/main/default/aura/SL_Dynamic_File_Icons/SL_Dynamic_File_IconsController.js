({
	init: function (component, event, helper) {
		var record = component.get('v.record');
		for (var i in record) {
			if (i == 'FileType' || i == 'FileExtension') {
				if (helper.typeToIconName()[record[i]]) {
					var fileType = helper.typeToIconName()[record[i]];
					component.set('v.normalizedType', fileType);
					component.set('v.fileTitle', fileType);
				} else {
					component.set('v.normalizedType', 'file');
					component.set('v.category', 'utility');
					component.set('v.fileTitle', 'Unknown file type');
				}
			}
		}
	}
})