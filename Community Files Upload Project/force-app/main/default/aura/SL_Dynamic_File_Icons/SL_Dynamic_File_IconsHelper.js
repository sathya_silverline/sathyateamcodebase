({
	typeToIconName: function () {
		return {
			pdf: 'pdf',
			PDF: 'pdf',
			jpg: 'image',
			JPG: 'image',
			png: 'image',
			PNG: 'image',
			ppt: 'ppt',
			xls: 'excel',
			XLS: 'excel',
			psd: 'psd',
			PSD: 'psd',
			doc: 'word',
			DOC: 'word',
			WORD_X: 'word',
			txt: 'word',
			TXT: 'word',
			rtf: 'word',
			RTF: 'word'
		}
	}
})