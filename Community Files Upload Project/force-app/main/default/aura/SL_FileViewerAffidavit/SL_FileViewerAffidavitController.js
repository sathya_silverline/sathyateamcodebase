({
	doInit : function(component, event, helper) {
        var action = component.get("c.fetchAffidavitDocument");
        action.setParams({ContactId : component.get('v.recordId')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.documentId', response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    } 
                }
             $A.get("e.force:refreshView").fire();
        });
       
        $A.enqueueAction(action);  
    }
})