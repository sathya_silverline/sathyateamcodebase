({
	init: function (component, event, helper) {
		helper.loadData(component);
	},
	inlineEdit: function (component, event) {
		var event = component.getEvent('communityFileShareEvent');
		event.setParams({
			type: 'inlineEdit',
			params: null
		});
		event.fire();
	},
	previewFile: function (component, event) {
		var Id = event.currentTarget.id;
		$A.get('e.lightning:openFiles').fire({
			recordIds: [Id]
		});
	},
	columnChange: function (component, event, helper) {
		var oldCol = event.getParam('oldValue');
		var col = event.getParam('value');
		if (oldCol != col) helper.loadData(component, true);
	},
	filterChange: function (component, event, helper) {
		helper.loadData(component);
	},
	directionChange: function (component, event, helper) {
		var oldDirection = event.getParam('oldValue');
		var direction = event.getParam('value');
		if (oldDirection != direction) helper.loadData(component);
	},
	communityShareHandler: function (component, event, helper) {
		var type = event.getParam('type');
		if (type == 'reloadFiles') helper.loadData(component);
	},
	handleSearchInput: function (component, event, helper) {
		var oldSearchText = event.getParam('oldValue');
		var searchText = event.getParam('value');
		if (oldSearchText !== searchText && searchText.length >= 3) {
			setTimeout(function () {
				helper.loadData(component);
			}, 300);
		}
		if (searchText.length == 0) helper.loadData(component);
	}
})