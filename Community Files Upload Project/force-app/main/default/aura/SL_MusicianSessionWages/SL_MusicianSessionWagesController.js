({	
    doInit : function(component,event,helper){
        console.log('inside Do init');
    },
    onfocus : function(component,event,helper){
        console.log('Inside Onfocus?>>>>>>');
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
        var getInputkeyWord = component.get("v.SearchKeyWord") != null ? component.get("v.SearchKeyWord") : '';
        console.log('getInputkeyWord>>>',getInputkeyWord);
        helper.searchHelper(component,event,getInputkeyWord);
    },
    onblur : function(component,event,helper){  
        console.log('Inside Pnblur?>>>>>>');
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
        console.log('Inside Keypresscontroller?>>>>>>'+component.get("v.SearchKeyWord"));
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part. 
        if(isNaN(getInputkeyWord)){
            console.log('isnot a number');
             var forError = component.find("ErrorMessageForIlligal");
             $A.util.addClass(forError, 'slds-show');
             $A.util.removeClass(forError, 'slds-hide');
        }else{
             var forError = component.find("ErrorMessageForIlligal");
             $A.util.removeClass(forError, 'slds-show');
             $A.util.addClass(forError, 'slds-hide');
        }  
        if( getInputkeyWord.length > 0 ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        console.log('Inside clear?>>>>>>');
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );   
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        console.log('Inside handleComponentEvent?>>>>>>');
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        console.log('Inside handleComponentEvent 1111>>>', selectedAccountGetFromEvent);
        component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
        console.log('Inside handleComponentEvent 11112222>>>', component.get("v.selectedRecord"));
        component.set("v.SearchKeyWord", selectedAccountGetFromEvent.Tax_ID_Number__c);
        
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    handleClickOnSave : function(component, event, helper) {
        console.log('Inside handleClickfor save>>>');
        console.log('Inside Controller>>>', JSON.stringify(component.get("v.selectedRecord")) );
        var SelectedWrapper = JSON.stringify(component.get("v.selectedRecord"));
        var wagess = JSON.stringify(component.get("v.Wages"));
        helper.saveWages(component,event,SelectedWrapper,wagess); 
        
        var forError = component.find("ErrorMessage1");
            $A.util.addClass(forError, 'slds-hide');
             $A.util.removeClass(forError, 'slds-show');
        
    },
    
    saveContact : function(component, event, helper) {
        helper.saveContactToDb(component, event);
    },
    
    handleSaveSuccess : function(cmp, event) {
        // Display the save status
        //cmp.set("v.saveState", "SAVED");
        console.log('Main Yahi hun..');
    },
    
    isRefreshed : function(component, event, helper) {
        console.log('Inside parent refresh');
    }
})