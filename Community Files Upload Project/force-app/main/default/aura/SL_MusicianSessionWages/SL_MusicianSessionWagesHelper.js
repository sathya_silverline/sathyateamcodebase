({
	searchHelper : function(component,event,getInputkeyWord) {
	  // call the apex class method 
     var action = component.get("c.fetchLookUpValues");
      // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
          $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                console.log('storeResponse>>>>',storeResponse);
                component.set("v.listOfSearchRecords", storeResponse);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
	},
    
    saveWages : function(component,event,SelectedWrapper,wages) {
        if(component.get("v.selectedRecord")==null){
            var forError = component.find("ErrorMessage1");
            $A.util.removeClass(forError, 'slds-hide');
             $A.util.addClass(forError, 'slds-show');
             
        }
        console.log('Inside Savewages>>>', JSON.stringify(component.get("v.selectedRecord")) );
        console.log('wages>>>', wages );
    	var action = component.get("c.SaveWagesWithContact");
      // set param to method  
        action.setParams({
           'objRelated' : SelectedWrapper,
            'sessionContractId' : component.get("v.recordId"),
            'Wages' : wages
          });
      // set a callBack    
        action.setCallback(this, function(response) { 
            if(response.getState() == 'SUCCESS'){
                console.log('Pitradev Sar');
                component.set("v.SearchKeyWord",null);
        		component.set("v.listOfSearchRecords", null );
        		component.set("v.selectedRecord", null );
                component.set("v.Wages", 0 );
                //location.reload();
                $A.get('e.force:refreshView').fire();
                //$A.enqueueAction(component.get('c.doInit'));
            }
            
        });   
        
        $A.enqueueAction(action);
    },
    
    saveContactToDb : function(component,event) {
        console.log('Inside Save Contacts>>>', component.get("v.contactToSave"));
    }
})