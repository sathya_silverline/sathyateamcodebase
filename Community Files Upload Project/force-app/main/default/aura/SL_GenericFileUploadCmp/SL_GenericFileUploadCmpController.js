({
    handleUploadFinished : function(component, event, helper) {
        
        component.set('v.lstFiles',event.getParam('files'));
    },
    
    handleUploadDocument : function(component, event, helper) {
        
        component.set('v.lstFiles',event.getParam('files'));
    }
})