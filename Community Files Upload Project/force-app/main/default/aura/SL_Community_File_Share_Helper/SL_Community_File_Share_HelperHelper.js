({
	createFilterString: function (arr) {
		var filterString = '';
		arr.forEach(function (item, index) {
			if (index > 0) filterString += ' AND';
			filterString += ' ' + item.fieldName + '=\'' + item.value + '\' ';
		});
		return filterString;
	},
	getFilterRecords: function (component) {
		var self = this;
		component.set('v.fetching', true);
		var fieldName = component.get('v.fieldName');
		var recId = component.get('v.recordId') || self.getUrlParam('ProjectID');;
		var communityVisible = component.get('v.communityVisibleFlag');
		var params = {
			component: component,
			controllerMethod: 'c.getRecordsForFilter',
			actionParameters: {
				fieldName: fieldName,
				recId: recId,
				communityVisible: communityVisible
			}
		};
		this.auraPromise(params)
			.then(function (ret) {
				component.set('v.records', JSON.parse(ret));
			})
			.catch(self.auraHandleErrors.bind(this, component));
	},
	getPicklistValues: function (component) {
		var self = this;
		var params = {
			component: component,
			controllerMethod: 'c.getPickListValues'
		};

		this.auraPromise(params)
			.then(function (resp) {
				component.set('v.picklistValues', resp);
			})
			.catch(self.auraHandleErrors.bind(this, component));
	},
	loadColumns: function (component) {
		var self = this;
		var fieldSet = component.get('v.fieldSetName');
		var params = {
			component: component,
			controllerMethod: 'c.getColumns',
			actionParameters: {
				fieldSet: fieldSet
			}
		};
		this.auraPromise(params)
			.then(function (ret) {
				if (!ret) return;
				var fields = ret.lstFieldset.map(function (item) {
					if (item.label == 'Owner ID') item.label = 'Uploaded By';
					return item;
				});
				component.set('v.columns', fields);
			})
			.catch(self.auraHandleErrors.bind(this, component));
	},
	loadData: function (component, reloadData) {
		var self = this;
		var recId = component.get('v.recordId') || self.getUrlParam('ProjectID');
		component.set('v.fetching', true);
		component.set('v.records', null);

		if (!recId) {
			component.set('v.noRecordId', true);
		}

		var defaultSorting = component.get('v.sortDirection');
		var sortingColumn = component.get('v.sortColumn');
		var fieldSet = component.get('v.fieldSetName');
		var communityVisible = component.get('v.communityVisibleFlag');
		var filterBy = component.get('v.filterBy');
		var searchText = component.get('v.searchText');
		if (reloadData) return;
		var params = {
			component: component,
			controllerMethod: 'c.getFiles',
			actionParameters: {
				fieldSet: fieldSet,
				recId: recId || null,
				defaultSorting: defaultSorting || null,
				sortingColumn: sortingColumn || null,
				communityVisible: communityVisible,
				filterBy: JSON.stringify(filterBy) || null,
				searchText: searchText
			}
		};

		var units = self.unitDivider()[component.get('v.defaultSizeUnits')];
		self.auraPromise(params)
			.then(function (ret) {
				if (!ret) {
					self.loadColumns(component);
					component.set('v.fetching', false);
					return;
				};
				component.set('v.records', null);
				var files = self.normilizeSizeAndAddProps(ret.files)(units)(
					ret.links
				);
				var columns = self.renameTypes(ret.fields);
				component.set('v.columns', columns);
				component.set('v.records', files);
				component.set('v.fetching', false);
			})
			.catch(self.auraHandleErrors.bind(this, component));
	},
	normilizeSizeAndAddProps: function (files) {
		return function (divider) {
			if (!divider) return files;
			files = files.map(function (file) {
				if (divider) file.ContentSize = +file.ContentSize / divider;
				return file;
			});
			return function (props) {
				if (!props) return files;
				return files.map(function (file) {
					props.forEach(function (prop) {
						if (prop.ContentDocumentId == file.ContentDocumentId)
							file.Visibility = prop.Visibility;
					});
					return file;
				});
			};
		};
	},
	renameObjectProp: function (objects, oldProp, newProp) {
		return objects.map(function (item) {
			for (var i = 0; i < item.length; i++) {
				item[i][newProp] = item[i][oldProp];
				delete item[i][oldProp];
			}
			return item;
		});
	},
	renameTypes: function (columns) {
		return columns.map(function (item) {
			if (item.fieldName === 'Owner.Name') item.fieldName = 'OwnerId';
			return item;
		});
	},
	sizeUnits: function (files, divider) {
		return files.map(function (file) {
			if (divider) file.ContentSize = +file.ContentSize / divider;
			return file;
		});
	},
	saveUpdatedRecords: function (component) {
		var self = this;
		var records = component.get('v.recordsToUpdate');

		var params = {
			component: component,
			controllerMethod: 'c.updateRecords',
			actionParameters: {
				records: JSON.stringify(records)
			}
		};
		this.auraPromise(params)
			.then(function (res) {
				if (res == 'success') {
					component.set('v.recordsToUpdate', []);
					component.set('v.inlineEdit', false);
					self.loadData(component);
				}
				component.set('v.fetching', false);
			})
			.catch(self.auraHandleErrors.bind(this, component));
	},
	unitDivider: function () {
		return {
			Kb: 1024,
			Byte: null,
			Mb: 1024 * 1024
		};
	},
	inArray: function (comparer, arr) {
		for (var i = 0; i < arr.length; i++) {
			if (comparer(arr[i])) return '' + i; //js understands 0 as false, so we need convert number to string to prevent false at 0 index
		}
		return null;
	}
})