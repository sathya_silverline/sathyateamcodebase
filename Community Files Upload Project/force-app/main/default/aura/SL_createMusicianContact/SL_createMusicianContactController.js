({
    
    handleLoad: function(cmp, event, helper) {
        cmp.set('v.showSpinner', false);
    },
    
    handleSubmit: function(cmp, event, helper) {
        cmp.set('v.disabled', true);
        cmp.set('v.showSpinner', true);
    },
    
    handleError: function(cmp, event, helper) {
        // errors are handled by lightning:inputField and lightning:messages
        // so this just hides the spinner
        cmp.set('v.showSpinner', false);
    },
    
    handleSuccess: function(cmp, event, helper) {
        var params = event.getParams();
        cmp.set('v.contactRecordId', params.response.id);
        cmp.set('v.showSpinner', false);
        cmp.set('v.saved', true);
        //location.reload();
    },
    
    saveContact : function(cmp, event, helper) {
        console.log('Inside Contact Save');
        console.log('Contact Object>>>', cmp.get('v.contactToSave'));
        console.log('Wages>>>',cmp.get('v.Wages'));
        console.log('SessionContractId>>>',cmp.get('v.recordId'));
        var cont =  JSON.stringify(cmp.get('v.contactToSave'));
        var wages = cmp.get('v.Wages');
        var sessionContaractId = cmp.get('v.recordId');
        console.log('Method Call update kiye hai');
        var action = cmp.get("c.saveContactAndSessionMusicianWages");
        action.setParams({
            'strContact' : cont,
            'sessionContractId' : sessionContaractId,
            'wages' : wages
        });
        // set a callBack    
        action.setCallback(this, function(response) { 
            if(response.getState() == 'SUCCESS'){
                console.log('inside Callback');
                 cmp.set("v.contactToSave",null);
        		cmp.set("v.Wages", 0 );
        		//component.set("v.selectedRecord", null );
        		 $A.get('e.force:refreshView').fire();
                
            }
        });
        
        $A.enqueueAction(action);
       
     },
    
     addContactRow : function(component, event, helper) {
        var forclose = component.find("newContactForm");
           $A.util.removeClass(forclose, 'slds-hide');
           $A.util.addClass(forclose, 'slds-show');
         
       var forbutton = component.find("newbuttonForm");
           $A.util.addClass(forbutton, 'slds-hide');
           $A.util.removeClass(forbutton, 'slds-show');  
        
    },
    
    isRefreshed : function(component, event, helper) {
        
    }
});