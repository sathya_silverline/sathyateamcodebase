({
	doInit : function(component, event, helper) {
        
		// component.set("v.FirstName", '');

		component.set("v.isInitSearch", true);
		// helper.getRecords(component, event, helper, 0);

	},
    
    getResult : function(component, event, helper){
		helper.getRecords(component, event, helper, 0);
    },

	clearSearch : function(component, event, helper){
		
		component.find("firstName").set("v.value", "");
	},

	sortByNameAsc : function(component, event, helper){
		helper.getRecords(component, event, helper, 0);
	},
	sortByNameDesc : function(component, event, helper){
		helper.getRecords(component, event, helper, 1);
	},
    
	moveToDetailPage : function(component, event, helper) {
		var recordId = event.target.title;
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		"recordId": recordId,
		"slideDevName": "detail"
		});
		navEvt.fire();
	},

	formkeyPress : function(component, event, helper) {
		if (event.keyCode === 13)
			helper.getRecords(component, event, helper, 0);
	},
})