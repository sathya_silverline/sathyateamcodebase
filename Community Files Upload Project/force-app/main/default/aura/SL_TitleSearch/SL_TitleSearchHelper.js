({
	getRecords : function(component, event, helper, sortOrder) {

		component.set("v.searching", true);
		component.set("v.sortOrder", sortOrder);
		var hContainer = component.find("headerContainer");
		var bContainer = component.find("bodyContainer");
		$A.util.addClass(hContainer, 'slds-hide');
		$A.util.addClass(bContainer, 'slds-hide');

		var FirstName = component.get("v.FirstName").trim();
		
		var action = component.get("c.getResults");
		action.setParams({
			"FirstName": (((FirstName.length >= 1) == true) ? FirstName : ""),
			"sortOrder": sortOrder
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var result = response.getReturnValue();
				component.set("v.results", result);

				if(result.length == 0 || result.length > 100){
					component.set("v.isError", true);
					component.set("v.errorMessage", "Has Error");
				}
				else{
					component.set("v.isError", false);
					component.set("v.errorMessage", "");
				}
			}
			else{
				component.set("v.results", null);
				component.set("v.isError", true);
				component.set("v.errorMessage", "Has Error");
			}
			
			component.set("v.searching", false);
			$A.util.removeClass(hContainer, 'slds-hide');
			$A.util.removeClass(bContainer, 'slds-hide');
        });
        $A.enqueueAction(action);
	},


	firstNameChange : function(component, event, helper) {
		var fnComp = component.find("firstName");
		var fnValue = fnComp.get("v.value");
        if(fnValue != null && fnValue != '' && fnValue.length < 3){
			fnComp.setCustomValidity("Please enter minimum 3 characters");
			fnComp.reportValidity();
			return false;
        }
        else{
			fnComp.setCustomValidity("");
			fnComp.reportValidity();
			return true;
		}
	},
	
	getParameterByNames: function(name, url) {
        if(!url){
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),results = regex.exec(url);
        if(!results){
            return null;
        }
        if(!results[2]){
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
})