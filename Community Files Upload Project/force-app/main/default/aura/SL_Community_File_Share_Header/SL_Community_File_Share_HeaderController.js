({
	init: function (component, event, helper) {
		var sortDirection = component.get('v.sortDirection');
		component.set('v.sorting', sortDirection == 'ASC' ? 'utility:arrowup' : 'utility:arrowdown');
		helper.loadColumns(component);
	},
	sortBy: function (component, event, helper) {
		var column = event.currentTarget.id;
		var sortDirection = component.get('v.sortDirection');
		var compEvent = component.getEvent("communityFileShareEvent");
		compEvent.setParams({
			type: 'sorting',
			params: {
				column: column,
				sortDirection: sortDirection == 'DESC' ? 'ASC' : 'DESC'
			}
		});
		compEvent.fire();
		component.set('v.sorting', component.get('v.sortDirection') == 'ASC' ? 'utility:arrowup' : 'utility:arrowdown');
	}
})