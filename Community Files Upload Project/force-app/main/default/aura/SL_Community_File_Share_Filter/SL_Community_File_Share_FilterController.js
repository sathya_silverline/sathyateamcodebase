({
	init: function (component, event, helper) {
		helper.getFilterRecords(component);
	},
	handleSelect: function (component, event, helper) {
		var selected = event.getParam('value');
		var fieldName = component.get('v.fieldName');
		var selectedValue = component.get('v.selectedValue');
		var compEvent = component.getEvent('communityFileShareEvent');
		compEvent.setParams({
			type: selected == selectedValue ? 'deleteFilterBy' : 'filterBy',
			params: {
				fieldName: fieldName,
				value: selected
			}
		});
		compEvent.fire();
		component.set('v.selectedValue', selected == selectedValue ? '' : selected);
	}
})