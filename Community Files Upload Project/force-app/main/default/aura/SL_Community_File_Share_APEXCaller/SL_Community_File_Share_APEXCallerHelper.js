({
    component: null,
    auraPromise: function (params) {
        return new Promise($A.getCallback(this.auraCallback.bind(null, params)));
    },
    auraCallback: function (params, resolve, reject) {
        var component = params.component;
        var action = component.get(params.controllerMethod);
        action.setParams(params.actionParameters || {});
        action.setCallback(
            this,
            $A.getCallback(function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var ret = response.getReturnValue();
                    return resolve(ret);
                } else {
                    return reject(response.getError());
                }
            })
        );
        $A.enqueueAction(action);
    },
    auraHandleErrors: function (component, err) {
        component.set('v.fetching', false);
        console.error(err);
        this.handleErrors(component, err);
    },
    getSingleMessage: function (errObj) {
        var errMsg = '';
        if (errObj.hasOwnProperty('message')) errMsg.concat(errObj.message, ' ');
        if (errObj.hasOwnProperty('pageErrors')) return errMsg.concat(errObj.pageErrors[0].message) || errMsg.concat(errObj.pageErrors.message, ' ');
        if (errObj.hasOwnProperty('fieldErrors')) return errMsg.concat(errObj.fieldErrors.message, ' ');
        return errMsg;
    },
    handleErrors: function (component, errors) {
        var self = this;
        var arr = [];
        if (errors && errors.length == 1) {
            var params = {
                component: component,
                type: 'error',
                message: self.getSingleMessage(errors[0]),
                showToast: true,
                title: 'Error'
            };
            self.showToast(params);
            return;
        } else {
            var params = {
                component: component,
                type: 'error',
                message: 'Oop! Something went wrong',
                showToast: true,
                title: 'Error'
            };
            self.showToast(params);
            return;
        }
        [errors].forEach(function (error) {
            for (var k in error) {
                if (error[k] && error[k].length > 0) {
                    Array.isArray(error[k]) && error[k].forEach(function (info) {
                        arr.push(info);
                    });
                }
            }
        });
        this.removeDuplicates(arr, 'statusCode').forEach(function (info) {
            var params = {
                component: component,
                type: 'error',
                message: info.message,
                showToast: true,
                title: 'Error'
            };
            self.showToast(params);
        });
    },
    removeDuplicates: function (myArr, prop) {
        return myArr.filter(function (obj, pos, arr) {
            return arr.map(function (mapObj) {
                return mapObj[prop]
            }).indexOf(obj[prop]) === pos;
        });
    },
    iconType: function (type) {
        var utility = 'utility:';
        switch (type) {
            case 'info':
                return ''.concat(utility, 'info');
            case 'success':
                return ''.concat(utility, 'success');
            case 'warning':
                return ''.concat(utility, 'warning');
            case 'error':
                return ''.concat(utility, 'error');
            default:
                return ''.concat(utility, 'info');
        }
    },
    showToast: function (params) {
        var isLightning = this.isLightning();
        var component = params.component;
        var type = params.type;
        var toastType = params.type;
        var message = params.message;
        var showToast = params.showToast;
        var title = params.title;
        this.component.set('v.iconType', this.iconType(type));

        if (isLightning || this.component.find('notifLib')) {
            if (this.component.find('notifLib')) {
                if (toastType === 'notice') {
                    this.component.find('notifLib').showNotice({
                        variant: type,
                        header: title || '',
                        message: message
                    });
                    return;
                }
                this.component.find('notifLib').showToast({
                    variant: type,
                    title: title || '',
                    message: message
                });
                return;

            }
            isLightning.setParams({
                "title": title,
                "type": type,
                "message": message
            });
            isLightning.fire();
        } else {
            component.set('v.type', type);
            component.set('v.message', message);
            component.set('v.showToast', showToast);
        }
    },
    isLightning: function () {
        return $A.get('e.force:showToast');
    },
    isMobDevice: function () {
        return $A.get('$Browser.isIPhone') ||
            $A.get('$Browser.isIOS') ||
            $A.get('$Browser.isAndroid') ||
            !$A.get('$Browser.isDesktop') ||
            $A.get('$Browser.isBlackBerry') ||
            $A.get('$Browser.isPhone') ||
            $A.get('$Browser.isWindowsPhone') ||
            $A.get('$Browser.isTablet') ||
            $A.get('$Browser.isIPad');
    },
    getElementByAttr: function (tagName, attrName, attrValue) {
        return [].filter.call(
            document.getElementsByTagName(tagName),
            function (el) {
                return el && el.getAttribute(attrName) == attrValue;
            }
        );
    },
    getUrlParam: function (param) {
        return this.getParameterPolyfill(param);
    },
    getParameterPolyfill: function (name, url) {
        name = name.replace('/[\[\]]/g', '\\$&');

        if (!url) url = window.location.href;

        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)', 'i'),
            results = regex.exec(url);
        if (!results) return null;

        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
});