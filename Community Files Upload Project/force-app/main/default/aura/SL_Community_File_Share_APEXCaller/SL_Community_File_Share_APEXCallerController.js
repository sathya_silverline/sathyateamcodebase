({
    hideToast: function (component) {
        component.set('v.showToast', false);
    },
    init: function (component, event, helper) {
        helper.component = component;
        helper.isLightning() && $A.createComponent(
            'lightning:notificationsLibrary',
            {
                "aura:id":'notifLib'
            },
            function(notifLib, status, errorMessage) {
                if (status === 'SUCCESS') {
                    var body = component.get('v.body');
                    body.push(notifLib);
                    component.set('v.body',body);
                }
                console.log(status, errorMessage);
            }
        );
    }
})