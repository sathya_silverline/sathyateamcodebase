({
    doInit: function (component, event, helper) {
        component.set('v.isMobDevice', helper.isMobDevice());
    },
    uploadFilesModal: function (component, event, helper) {
        helper.uploadFilesModal(component);
    },
    eventHandler: function (component, event, helper) {
        var type = event.getParam('type');
        var params = event.getParam('params');
        helper.handleEvent(component, type, params);
    },
    handleSave: function (component, event, helper) {
        component.set('v.fetching', true);
        helper.saveUpdatedRecords(component);
    },
    handleCancel: function (component) {
        component.set('v.recordsToUpdate', []);
        component.set('v.inlineEdit', false);
    },
    liveSearch: function (component, event, helper) {
        var searchBlock = component.find('searchBlock');
        $A.util.addClass(searchBlock, 'slds-is-searching');
    },
    handleClearSearch: function (component, event, helper) {
        var searchBlock = component.find('searchBlock');
        component.find('stext').set('v.value', '');
        $A.util.removeClass(searchBlock, 'slds-is-searching');
    }
});