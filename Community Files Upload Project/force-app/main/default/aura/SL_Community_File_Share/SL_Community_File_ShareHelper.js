({
    handleEvent: function (component, type, params) {
        var self = this;
        switch (type) {
            case 'sorting':
                var column = params.column;
                var sortDirection = params.sortDirection;
                component.set('v.sortColumn', column);
                component.set('v.sortDirection', sortDirection);
                break;
            case 'inlineEdit':
                component.set('v.inlineEdit', true);
                break;
            case 'filterBy':
                var fieldName = params.fieldName;
                var value = params.value;
                var newParams = {
                    fieldName: fieldName,
                    value: value
                };
                var filterFields = component.get('v.filterBy');
                var inArray = self.inArray(function (e) {
                    return e.fieldName === newParams.fieldName
                }, filterFields);
                if (!inArray) {
                    filterFields.push(newParams);
                } else {
                    filterFields[+inArray] = newParams;
                }
                component.set('v.filterBy', self.removeDuplicates(filterFields, 'fieldName'));
                break;
            case 'deleteFilterBy':
                var fieldName = params.fieldName;
                var value = params.value;
                var filterFields = component.get('v.filterBy');
                var filteredRecords = filterFields.filter(function (item) {
                    return item.fieldName != fieldName;
                });
                component.set('v.filterBy', filteredRecords);
                break;
            case 'recordsToUpdate':
                var recId = component.get('v.recordId') || this.getUrlParam('ProjectID');
                var id = params.id;
                var value = params.value;
                var fieldName = params.fieldName;
                var ContentDocumentId = params.ContentDocumentId;
                var records = component.get('v.recordsToUpdate');
                records.push({
                    id: id,
                    ContentDocumentId: ContentDocumentId,
                    value: value,
                    fieldName: fieldName,
                    recId: recId
                });
                component.set('v.recordsToUpdate', records);
                break;
            default:
                console.log(type, params);
                break;
        }
    },
    uploadFilesModal: function (component) {
        var recId = component.get('v.recordId') || this.getUrlParam('ProjectID');
        $A.createComponent(
            'c:SL_Community_File_Share_Popup', {
                popupTitle: 'Choose file category',
                communityVisibleFlag: component.get('v.communityVisibleFlag'),
                recId: recId
            },
            function (popup, status, errorMSG) {
                if (status === 'SUCCESS' && component.isValid()) {
                    var target = component.find('modal-popup');
                    var body = target.get('v.body');
                    body.push(popup);
                    target.set('v.body', body);
                } else if (status === 'ERROR') {
                    console.log(errorMSG);
                } else if (status === 'INCOMPLETE') {
                    console.log('No response');
                }
            }
        );
    }
});