({
    closeMe: function (component) {
        component.destroy();
    },
    getPicklistValues: function (component) {
        var params = {
            component: component,
            controllerMethod: 'c.getPickListValues'
        };
        var picklistProm = this.auraPromise(params);
        picklistProm.then(function (resp) {
            component.set('v.picklistValues', resp);
        });
    },
    isCommunity: function (component) {
        var params = {
            component: component,
            controllerMethod: 'c.isCommunity'
        };
        var isCommunityProm = this.auraPromise(params);
        isCommunityProm.then(function (resp) {
            var modalWrapper = component.find('modal-wrapper');
            var modal = component.find('modal');
            var backdrop = component.find('backdrop');
            if (resp == 'not-community') {
                $A.util.addClass(modalWrapper, 'slds-modal--fix-noncommunity');
                $A.util.addClass(modal, 'slds-modal--foregrounded--fix-noncommunity');
                $A.util.addClass(backdrop, 'slds-backdrop--foregrounded--fix-noncommunity');
            }
        });
    },
    changeCategory: function (component, event) {
        var val = event.getSource().get('v.value');
        component.set('v.selectedValue', val);
    }
});