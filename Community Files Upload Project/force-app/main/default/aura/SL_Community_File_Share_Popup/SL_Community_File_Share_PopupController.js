({
    init: function (component, event, helper) {
        helper.getPicklistValues(component);
        helper.isCommunity(component);
    },
    closeMe: function (component, event, helper) {
        helper.closeMe(component);
    },
    changeCategory: function (component, event, helper) {
        helper.changeCategory(component, event);
    },
    enableInCommunity: function (component, event, helper) {
        var communityEnabled = component.get('v.communityEnabled');
        component.set('v.communityEnabled', !communityEnabled);
    },
    handleUploadFinished: function (component, event, helper) {
        var modalWrapper = component.find('modal-wrapper');
        $A.util.addClass(modalWrapper, 'slds-hide');

        var uploadedFiles = event.getParam('files');
        var communityEnabled = component.get('v.communityVisibleFlag') ? true : component.get('v.communityEnabled');
        var fileCategory = component.get('v.selectedValue');
        var filesIds = uploadedFiles.map(function (item) {
            return item.documentId;
        });
        var compEvent = $A.get('e.c:SL_Community_File_Share_Event');
        var params = {
            component: component,
            controllerMethod: 'c.updateFiles',
            actionParameters: {
                filesIds: filesIds,
                communityEnabled: communityEnabled,
                fileCategory: fileCategory || null
            }
        };
        var updatePromise = helper.auraPromise(params);
        updatePromise.then(function (resp) {
            compEvent.setParams({
                type: 'reloadFiles',
                params: null
            });
            compEvent.fire();
            helper.closeMe(component);
        });
    },
    backStep: function (component, event, helper) {
        var currentStep = component.get('v.step');
        var communityVisible = component.get('v.communityVisibleFlag');
        console.log(currentStep);
        switch (currentStep) {
            case 'upload':
                if (!communityVisible) {
                    component.set(
                        'v.popupTitle',
                        'Choose visibility for external users with access'
                    );
                    component.set('v.stepLabel', 'Next');
                    component.set('v.step', 'community-visible');
                } else {
                    component.set('v.step', 'file-category');
                    component.set('v.stepLabel', 'Next');
                    component.set(
                        'v.popupTitle',
                        'Choose file category'
                    );
                }
                break;
            case 'community-visible':
                component.set('v.step', 'file-category');
                component.set('v.stepLabel', 'Next');
                component.set(
                    'v.popupTitle',
                    'Choose file category'
                );
                break;
            default:
                break;
        }

    },
    changeStep: function (component, event, helper) {
        var currentStep = component.get('v.step');
        var communityVisible = component.get('v.communityVisibleFlag');
        console.log(currentStep);
        switch (currentStep) {
            case 'file-category':
                if (!communityVisible) {
                    component.set(
                        'v.popupTitle',
                        'Choose visibility for external users with access'
                    );
                    component.set('v.stepLabel', 'Next');
                    component.set('v.step', 'community-visible');
                } else {
                    component.set('v.popupTitle', 'Upload your files');
                    component.set('v.stepLabel', 'Upload');
                    component.set('v.step', 'upload');
                }
                break;
            case 'community-visible':
                component.set('v.popupTitle', 'Upload your files');
                component.set('v.stepLabel', 'Next');
                component.set('v.step', 'upload');
                break;
            case 'upload':
                component.set('v.popupTitle', 'Your files were uploaded');
                component.set('v.stepLabel', 'Close');
                break;
            default:
                break;
        }
    }
});