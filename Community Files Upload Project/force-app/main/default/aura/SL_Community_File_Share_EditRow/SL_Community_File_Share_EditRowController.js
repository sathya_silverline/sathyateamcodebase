({
	init: function (component, event, helper) {
		helper.getPicklistValues(component);
		helper.loadData(component);
	},
	handleInlineEdit: function (component, event) {
		var id = event
			.getSource()
			.get('v.name')
			.split(',');
		var value = event.getSource().get('v.value');
		var event = component.getEvent('communityFileShareEvent');
		event.setParams({
			type: 'recordsToUpdate',
			params: {
				id: id[0],
				value: value == 'choose category...' ? null : value,
				fieldName: id[1]
			}
		});
		event.fire();
	},
	handleInlineCommunityVisible: function (component, event, helper) {
		var contentDocumentId = event.getSource().get('v.name');
		var value = event.getSource().get('v.checked');
		var event = component.getEvent('communityFileShareEvent');
		event.setParams({
			type: 'recordsToUpdate',
			params: {
				ContentDocumentId: contentDocumentId,
				value: value,
				fieldName: 'ContentDocumentLink'
			}
		});
		event.fire();
	},
	handleChangeTitle: function (component, event, helper) {
		var value = event.getSource().get('v.value');
		var id = event.getSource().get('v.name');
		var event = component.getEvent('communityFileShareEvent');
		event.setParams({
			type: 'recordsToUpdate',
			params: {
				id: id,
				value: value,
				fieldName: 'Title'
			}
		});
		event.fire();
	},
	columnChange: function (component, event, helper) {
		var oldCol = event.getParam('oldValue');
		var col = event.getParam('value');
		if (oldCol != col) helper.loadData(component, true);
	},
	directionChange: function (component, event, helper) {
		var oldDirection = event.getParam('oldValue');
		var direction = event.getParam('value');
		if (oldDirection != direction) helper.loadData(component);
	},
	communityShareHandler: function (component, event, helper) {
		var type = event.getParam('type');
		if (type === 'reloadFiles') helper.loadData(component);
	},
	filterChange: function (component, event, helper) {
		helper.loadData(component);
	},
	handleSearchInput: function (component, event, helper) {
		var oldSearchText = event.getParam('oldValue');
		var searchText = event.getParam('value');
		if (oldSearchText !== searchText && searchText.length >= 3) {
			setTimeout(function () {
				helper.loadData(component);
			}, 300);
		}
		if (searchText.length == 0) helper.loadData(component);
	}
})