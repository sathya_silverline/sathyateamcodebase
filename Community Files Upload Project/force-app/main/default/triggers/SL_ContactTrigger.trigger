trigger SL_ContactTrigger on Contact (before insert, before update) {

    if (Trigger.isUpdate && Trigger.IsBefore)
        SL_CommonTriggerHandler.updateBenificiaryStatus(trigger.new, trigger.oldMap);
}