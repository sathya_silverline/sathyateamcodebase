trigger SL_TitleTrigger on Title__c (before insert, After insert) {
	
    if (Trigger.isInsert && Trigger.IsAfter)
        SL_CommonTriggerHandler.createCollectionRecord(trigger.new);
}