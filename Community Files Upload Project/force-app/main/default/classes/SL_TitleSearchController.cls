public class SL_TitleSearchController {
    @AuraEnabled
   public static List<Title__c> getResults(String FirstName, Integer sortOrder){

       System.debug('SERVER SIDE ACTION CALLED>>>>>');

       Map<Integer, String> indexToSortOrder = new Map<Integer, String>{0 => ' ORDER BY Name ASC', 1 => ' ORDER BY Name DESC'};

       String whereClause = ' ';
       
       if(String.isNotBlank(FirstName)){
           FirstName = '%' + FirstName + '%';
           whereClause += 'Title_Name__c LIKE :FirstName ';
       }


       List<Title__c> Titles = new List<Title__c>();
       // List<Record> records = new List<Record>();

       String fieldsToQuery = 'Id, Title_Name__c ';
       System.debug('QUERY>>>> ' + 'SELECT ' + fieldsToQuery + ' FROM Title__c WHERE ' + whereClause + indexToSortOrder.get(Integer.valueOf(sortOrder)));
       Titles = Database.query('SELECT ' + fieldsToQuery + ' FROM Title__c WHERE ' + whereClause + indexToSortOrder.get(Integer.valueOf(sortOrder)));

       return Titles;
   }
}