public class SL_CommonTriggerHandler {

    public static void updateBenificiaryStatus(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts){
	
        Set<Id> setIdsMusiciansDied = new Set<Id>(); 
        List<Contact> lstBeneficiariesToUpdate = new List<Contact>(); 
		
		Id musicianRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Musician').getRecordTypeId();
		
        for(Contact objContact: lstNewContacts){
            if(objContact.RecordtypeId == musicianRecordTypeId && mapOldContacts.get(objContact.Id).Deceased_Date__c != objContact.Deceased_Date__c && objContact.Deceased_Date__c != NULL && 
			objContact.Source_of_Notification_Types__c == 'Phone Call'){
                setIdsMusiciansDied.add(objContact.Id);
            }
        }
		
		if(!setIdsMusiciansDied.isEmpty()) {
		
			for(Contact objContact: [	SELECT Bene_Status__c 
										FROM Contact 
										WHERE Musician__c IN: setIdsMusiciansDied 
										AND Bene_Type__c = 'PRI'
									]) {
			
				objContact.Bene_Status__c = 'Pending';
				lstBeneficiariesToUpdate.add(objContact);
			}
			
			if(!lstBeneficiariesToUpdate.isEmpty())
				update lstBeneficiariesToUpdate; 
		}
    }
    
    public static void createCollectionRecord(List<Title__c> lstNewTitles){
        
        List<Collection__c> lstCollectionToCreate = new List<Collection__c>(); 
        
        for(Title__c objTitle : lstNewTitles) {
            if(objTitle.Signatory_Entity__c != null){
                
                lstCollectionToCreate.add(new  Collection__c(Title_Name__c = objTitle.Id, Obligated_Party__c = objTitle.Signatory_Entity__r.Id, Status__c = 'New'));
            }
        }
        
        insert lstCollectionToCreate;
    }
}