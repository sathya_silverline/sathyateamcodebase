public class SL_FileViewer {
    @AuraEnabled 
    public static Id fetchContentDocument(String ContactId){
        contentDocument objContentDocument = new ContentDocument();
        objContentDocument = [Select Id, Title, FileType, CreatedBy.Name, ContentSize From contentDocument WHERE Title='3_Musician'];
        Contact objContact = new Contact();
        objContact = [SELECT Id, NuMu_W4_Sent_Date__c FROM Contact WHERE Id=:ContactId];
        objContact.NuMu_W4_Sent_Date__c = date.today();
        update objContact;
        return objContentDocument.Id;  
    }
    
    @AuraEnabled 
    public static Id fetchAffidavitDocument(String ContactId){
        if(ContactId != null) {
            Contact objContact = new Contact(Id = ContactId, Send_BCP_Email__c = true);
            update objContact;
        }
        
        List<ContentDocument> lstContentDocuments = [Select Id, Title, FileType, CreatedBy.Name, ContentSize From ContentDocument WHERE Title='Affidavit Template_ Blank'];
        
        if(!lstContentDocuments.isEmpty())
        	return lstContentDocuments[0].Id;  
        else
            return null;
    }
    
    @AuraEnabled 
    public static Id fetchDRL(String strContactId) {
        
        if(strContactId != null) 
            update new Contact(Id = strContactId, Send_DRL_Email__c = true);
        
        List<ContentDocument> lstContentDocuments = [Select Id, Title, FileType, CreatedBy.Name, ContentSize From ContentDocument WHERE Title='DRL Email Template'];
        
        if(!lstContentDocuments.isEmpty())
        	return lstContentDocuments[0].Id;  
        else
            return null;
    }
    
    @AuraEnabled 
    public static Id fetchA60Document(String CollectionId){
        if(CollectionId != null) {
            Collection__c objCollection = new Collection__c(Id = CollectionId, A60_sent_date__c = system.today(), Status__c = 'In Progress');
            update objCollection;
        }
        
        List<ContentDocument> lstContentDocuments = [Select Id, Title, FileType, CreatedBy.Name, ContentSize From ContentDocument WHERE Title='5_Collections'];
        
        if(!lstContentDocuments.isEmpty())
        	return lstContentDocuments[0].Id;  
        else
            return null;
    }
}