public class SL_MusicianSessionWegesController {
    @AuraEnabled
    public static List < Contact > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < Contact > returnList = new List < Contact > ();
        Id RecTypeId = '0123i000000TtHMAA0';
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name, Tax_ID_Number__c, MailingStreet, MailingCity, MailingState, MailingPostalCode from ' +ObjectName + ' where Tax_ID_Number__c LIKE: searchKey AND RecordTypeId =:RecTypeId order by createdDate DESC limit 5';
        List < Contact > lstOfRecords = Database.query(sQuery);
        System.debug('>>>sQuery>>'+sQuery);
        System.debug('>>>lstOfRecords>>'+lstOfRecords);
        /*for (Contact obj: lstOfRecords) {
            System.debug('>>>Inside Loop>>' + obj);
            returnList.add(obj);
        }*/
        System.debug('>>>List>>'+returnList);
        return lstOfRecords;
    }
    
    @AuraEnabled
    public static Boolean SaveWagesWithContact(String objRelated, String sessionContractId, String Wages) {
        system.debug('InsideSaveWages>>>>>>>>>>>>'+ objRelated);
        system.debug('wages-->' + Wages);
        Contact objContact = (Contact)System.JSON.deserialize(objRelated, Contact.class);
        Integer intWages = (Integer)System.JSON.deserialize(Wages, Integer.class);
        system.debug('objContact-->' + objContact);
        Musician_Session_Wage__c obMSW = new Musician_Session_Wage__c();
        if(objContact == null){
            system.debug('>>>inside if>>>');
        	obMSW.Session_Contract__c = sessionContractId;
            obMSW.Wages__c = intWages;
        }else{
            system.debug('>>>inside if>>>');
            obMSW.Musician__c = objContact.Id; 
            obMSW.Session_Contract__c = sessionContractId;
            obMSW.Wages__c = intWages;
        }
        
		Insert obMSW;
        //system.debug('objWrapper-->' + objWrapper);
        Return true;
    }
    
    Public Class Wrapper{
      @AuraEnabled
      Public Contact objContact {get; set;} 
      @AuraEnabled
      Public Decimal Wages {get; set;}  
        Public Wrapper(Contact objContact, Decimal Wages){
            this.objContact = objContact;
            this.Wages = Wages;
        }
    }
}