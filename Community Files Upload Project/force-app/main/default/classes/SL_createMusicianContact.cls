public class SL_createMusicianContact {
	@AuraEnabled
    public static Boolean saveContactAndSessionMusicianWages(String strContact, String sessionContractId, Integer wages) {//
         system.debug('inside the save>>>');
        //String strContact; String sessionContractId; Integer wages;
        system.debug('strContact>>>'+strContact);
        system.debug('wages>>>'+wages);
        system.debug('sessionContractId>>>'+sessionContractId);
        
        Contact objContact = (Contact)System.JSON.deserialize(strContact,Contact.class);
        objContact.Status__c = 'Potential';
        system.debug('objContact>>>'+objContact);
        Insert objContact;
        
        Musician_Session_Wage__c obMSW = new Musician_Session_Wage__c(Musician__c = objContact.Id, Session_Contract__c = sessionContractId, Wages__c = wages);
        Insert obMSW;
        system.debug('obMSW>>>'+obMSW);
        return true;
    }
}