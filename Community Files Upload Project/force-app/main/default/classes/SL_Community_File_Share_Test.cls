@isTest
public class SL_Community_File_Share_Test {

    public class ContactDefaults implements SL_TestDataFactory.FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Contact.FirstName => 'John2',
				Contact.LastName => 'Black'
			};
		}
	}

	public class ContentVersionDefaults implements SL_TestDataFactory.FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				ContentVersion.title=>'Test_abcdefg',
				ContentVersion.PathOnClient =>'test1',
				ContentVersion.VersionData => EncodingUtil.base64Decode('Unit Test Body')
			};
		}
	}

	public class ContentDocumentLinkDefaults implements SL_TestDataFactory.FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				ContentDocumentLink.ShareType => 'V',
				ContentDocumentLink.Visibility => 'InternalUsers'
			};
		}
	}
	public class ContentDocumentLinkAllUsers implements SL_TestDataFactory.FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				ContentDocumentLink.ShareType => 'V',
				ContentDocumentLink.Visibility => 'AllUsers'
			};
		}
	}

    private static Contact cont {
        get {
            if (cont == null) {
                cont = (Contact)SL_TestDataFactory.createSObject(new Contact(), 'SL_Community_File_Share_Test.ContactDefaults');
            }
            return cont;
        }
        set;
    }

    private static ContentVersion testFile {
        get {
            if (testFile == null) {
                testFile = (ContentVersion)SL_TestDataFactory.createSObject(new ContentVersion(), 'SL_Community_File_Share_Test.ContentVersionDefaults');
            }
            return testFile;
        }
        set;
    }

    private static ContentDocumentLink fileLinkDefaults {
        get {
            if (fileLinkDefaults == null) {
                fileLinkDefaults = (ContentDocumentLink)SL_TestDataFactory.createSObject(new ContentDocumentLink(), 'SL_Community_File_Share_Test.ContentDocumentLinkDefaults');
            }
            return fileLinkDefaults;
        }
        set;
    }

    private static ContentDocumentLink fileLinkAllUsers {
        get {
            if (fileLinkAllUsers == null) {
                fileLinkAllUsers = (ContentDocumentLink)SL_TestDataFactory.createSObject(new ContentDocumentLink(), 'SL_Community_File_Share_Test.ContentDocumentLinkAllUsers');
            }
            return fileLinkAllUsers;
        }
        set;
    }

    @isTest
    static void test_getPickListValues() {
        List<Map<String,String>> values = SL_Community_File_Share.getPickListValues();
        System.assert(values.size() > 0, 'Values for picklist must be present');
        for (Map<String,String> value : values) {
            System.assert(value.keySet().contains('label'), 'picklist must have label');
            System.assert(value.keySet().contains('value'), 'picklist must have value');
        }
    }

    @isTest
    static void test_getFilesIternal() {
        insert cont;
        insert testFile;
        ContentVersion insertedFile = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =:testFile.Id LIMIT 1];
        fileLinkDefaults.LinkedEntityId = cont.Id;
        fileLinkDefaults.ContentDocumentId = insertedFile.ContentDocumentId;
        insert fileLinkDefaults;

        Schema.DescribeSObjectResult objDescribe = ContentVersion.sObjectType.getDescribe();
    	Map<String,Schema.FieldSet> fieldSets = objDescribe.fieldSets.getMap();
        System.assert(fieldSets.keySet().size() > 0, 'you need to setup some fieldsets');
        String fieldsetName = new List<String>(fieldSets.keySet())[0];

        List<SL_Community_File_Share.ArgumentsWrapper> filters = new List<SL_Community_File_Share.ArgumentsWrapper>();
        SL_Community_File_Share.ArgumentsWrapper filter = new SL_Community_File_Share.ArgumentsWrapper();
        filter.fieldName = 'Title';
        filter.value = 'Test_abcdefg';
        filters.add(filter);

        SL_Community_File_Share.ResponseWrapper files = SL_Community_File_Share.getFiles(
            fieldsetName,
            cont.Id,
            null,
            ' ORDER BY Title ',
            false,
            JSON.serialize(filters),
            ''
        );
        System.assertEquals(1, files.files.size(), 'Test inserts only one file');
        for (ContentVersion file : files.files) System.assertEquals(testFile.Title, file.Title, 'file title need to be ' + testFile.Title);
    }

    @isTest
    static void test_getFilesCommunity() {
        insert cont;
        insert testFile;
        ContentVersion insertedFile = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =:testFile.Id LIMIT 1];
        fileLinkAllUsers.LinkedEntityId = cont.Id;
        fileLinkAllUsers.ContentDocumentId = insertedFile.ContentDocumentId;
        insert fileLinkAllUsers;

        Schema.DescribeSObjectResult objDescribe = ContentVersion.sObjectType.getDescribe();
    	Map<String,Schema.FieldSet> fieldSets = objDescribe.fieldSets.getMap();
        System.assert(fieldSets.keySet().size() > 0, 'you need to setup some fieldsets');
        String fieldsetName = new List<String>(fieldSets.keySet())[0];
        List<Id> soslIds = new List<Id>{insertedFile.Id};
        Test.setFixedSearchResults(soslIds);
        SL_Community_File_Share.ResponseWrapper files = SL_Community_File_Share.getFiles(
            fieldsetName,
            cont.Id,
            null,
            ' ORDER BY Title ',
            false,
            null,
            'abcd'
        );
        System.assertEquals(1, files.files.size(), 'Test inserts only one file');
        for (ContentVersion file : files.files) System.assertEquals(testFile.Title, file.Title, 'file title need to be ' + testFile.Title);
    }

    @isTest
    static void test_isCommunity() {
        System.assertEquals('not-community', SL_Community_File_Share.isCommunity(), 'need to be not community');
    }

    @isTest
    static void test_updateRecords() {
        List<Map<String,String>> pickListValues = SL_Community_File_Share.getPickListValues();
        insert cont;
        List<ContentVersion> testFiles = createTestFiles(2);
        insert testFiles;
        Set<Id> ids = (new Map<Id, ContentVersion>(testFiles)).keySet();
        List<ContentVersion> insertedFiles = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :ids];

        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        for (ContentVersion forLink : insertedFiles) {
            fileLinkDefaults = null;
            fileLinkDefaults.LinkedEntityId = cont.Id;
            fileLinkDefaults.ContentDocumentId = forLink.ContentDocumentId;
            links.add(fileLinkDefaults);
        }
        insert links;
        String json = JSON.serialize(
            generateArguments(
                cont.Id,
                new List<Id>(ids),
                pickListValues
            )
        );
        SL_Community_File_Share.updateRecords(json);
        List<ContentVersion> files = [SELECT Id, SL_FileCategory__c, ContentDocumentId FROM ContentVersion WHERE Id IN :ids];
        for (ContentVersion file : files) {
            System.assertEquals(pickListValues[0].get('value'), file.SL_FileCategory__c, 'file category need to be updated');
        }

    }


    private static List<ContentVersion> createTestFiles (Integer ctn) {
        List<ContentVersion> testFiles = new List<ContentVersion>();
        String testBody = 'Unit Test Body';
        for (Integer i=0;i<ctn;i++) {
            testFile=null;
            ContentVersion file = testFile;
            file.Title = file.Title += String.valueOf(i);
            file.PathOnClient = file.PathOnClient+=String.valueOf(i);
            file.VersionData = EncodingUtil.base64Decode(testBody+=String.valueOf(i));
            testFiles.add(file);
        }

        return testFiles;
    }

    private static List<SL_Community_File_Share.ArgumentsWrapper> generateArguments(
        Id contId,
        List<Id> contentVersionIds,
        List<Map<String,String>> pickListValues
    ) {
        List<SL_Community_File_Share.ArgumentsWrapper> arguments = new List<SL_Community_File_Share.ArgumentsWrapper>();

        List<ContentVersion> files = [SELECT Id, SL_FileCategory__c, ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersionIds];
        for (ContentVersion file : files) {
            SL_Community_File_Share.ArgumentsWrapper pickList = new SL_Community_File_Share.ArgumentsWrapper();
            pickList.id = file.Id;
            pickList.fieldName = 'SL_FileCategory__c';
            pickList.value = pickListValues[0].get('value');
            arguments.add(pickList);
            SL_Community_File_Share.ArgumentsWrapper cdl = new SL_Community_File_Share.ArgumentsWrapper();
            cdl.ContentDocumentId = file.ContentDocumentId;
            cdl.fieldName = 'ContentDocumentLink';
            cdl.value = Math.random() > 0.5 ? 'true' : 'false';
            cdl.recId = contId;
            arguments.add(cdl);
        }
        return arguments;
    }

    @isTest
    static void test_updateFiles() {
        List<Map<String,String>> pickListValues = SL_Community_File_Share.getPickListValues();
        String val = pickListValues[0].get('value');

        insert cont;
        List<ContentVersion> testFiles = createTestFiles(3);
        insert testFiles;
        Set<Id> ids = (new Map<Id, ContentVersion>(testFiles)).keySet();
        List<ContentVersion> insertedFiles = [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :ids];
        List<Id> contentDocIds = new List<Id>();
        for (ContentVersion file : insertedFiles) {
            contentDocIds.add(file.ContentDocumentId);
        }

        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        for (ContentVersion forLink : insertedFiles) {
            fileLinkDefaults = null;
            fileLinkDefaults.LinkedEntityId = cont.Id;
            fileLinkDefaults.ContentDocumentId = forLink.ContentDocumentId;
            links.add(fileLinkDefaults);
        }
        insert links;

        SL_Community_File_Share.updateFiles(contentDocIds, true, val);
        List<ContentVersion> afterUpdate = [SELECT SL_FileCategory__c FROM ContentVersion WHERE ContentDocumentId IN :contentDocIds];
        for (ContentVersion file : afterUpdate) {
            System.assertEquals(val, file.SL_FileCategory__c, 'SL_FileCategory__c to be ' + val);
        }
        List<ContentDocumentLink> updatedLinks = [SELECT Visibility FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocIds];
        for (ContentDocumentLink link : updatedLinks) {
            System.assertEquals('AllUsers', link.Visibility, 'All users visibility!!!');
        }
    }
}