/**
 * Created by D.Bondarenko
 * SLCOMMUNIT-417
 */

public with sharing class SL_Community_File_Share {

    @TestVisible
    class ArgumentsWrapper {
        public String id {get;set;}

        public String ContentDocumentId {get;set;}

        public String fieldName {get;set;}

        public String value {get;set;}

        public String recId {get;set;}
    }

    @AuraEnabled
    public static String updateRecords(String records){
        List<ArgumentsWrapper> castedRecords = (List<ArgumentsWrapper>)JSON.deserialize(records, List<ArgumentsWrapper>.class);
        List<ArgumentsWrapper> linksToUpdate = new List<ArgumentsWrapper>();

        Set<String> fields = new Set<String>();
        Set<String> ids = new Set<String>();
        fields.add('Id');
        for (ArgumentsWrapper record : castedRecords) {
            if (record.fieldName == 'ContentDocumentLink') {
                linksToUpdate.add(record);
            } else {
                fields.add(record.fieldName);
                ids.add(record.id);
            }
        }
        if (linksToUpdate.size() > 0) {
            updateLinks(linksToUpdate);
        }

        String soqlQuery = 'SELECT ' + String.join(new List<String>(fields), ',');
        soqlQuery += ' FROM ContentVersion';
        soqlQuery += ' WHERE Id IN :ids';

        List<ContentVersion> docs = Database.query(soqlQuery);
        for (ContentVersion doc : docs) {
            for (ArgumentsWrapper record : castedRecords) {
                if (doc.Id == record.Id) {
                    doc.put(record.fieldName,record.value);
                }
            }
        }
        update docs;
        return 'success';
    }

    private static void updateLinks(List<ArgumentsWrapper> linksToUpdate) {
        Set<Id> contentDocumentIds = new Set<Id>();
        Set<Id> linkedEntityId = new Set<Id>();
        for (ArgumentsWrapper link : linksToUpdate) {
            contentDocumentIds.add(link.ContentDocumentId);
            linkedEntityId.add(link.recId);
        }
        List<ContentDocumentLink> links = [SELECT Visibility, LinkedEntityId, ContentDocumentId
                                           FROM ContentDocumentLink
                                           WHERE ContentDocumentId IN :contentDocumentIds
                                           AND LinkedEntityId IN :linkedEntityId];
        for (ContentDocumentLink link : links) {
            for (ArgumentsWrapper linkToUpdate : linksToUpdate) {
                if (
                    link.ContentDocumentId == linkToUpdate.ContentDocumentId &&
                    link.LinkedEntityId == linkToUpdate.recId
                ) {
                    link.Visibility = linkToUpdate.value == 'true' ? 'AllUsers' : 'InternalUsers';
                }
            }
        }
        update links;
    }

    @AuraEnabled
    public static String isCommunity(){
        Id siteId = Site.getSiteId();
        if (siteId != null) {
            return 'community';
        }
        return 'not-community';
    }

    @AuraEnabled
    public static List<Map<String,String>> getPickListValues() {
        List<Map<String,String>> options = new List<Map<String,String>>();

        Schema.DescribeFieldResult fieldResult = ContentVersion.SL_FileCategory__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            Map<String, String> option = new Map<String, String>();
            option.put('label',f.getLabel());
            option.put('value', f.getValue());
            options.add(option);
        }
        return options;
    }

    @AuraEnabled
    public static String updateFiles(
        List<Id> filesIds,
        Boolean communityEnabled,
        String fileCategory
    ) {
        Set<Id> uniqueFiles = new Set<Id>(filesIds);
        if (communityEnabled) {
            List<ContentDocumentLink> links = [SELECT Id, Visibility FROM ContentDocumentLink WHERE ContentDocumentId IN :uniqueFiles];
            for (ContentDocumentLink link : links) link.Visibility = 'AllUsers';
            update links;
        }
        if (String.isNotBlank(fileCategory)) {
            List<ContentVersion> files = [SELECT Id, SL_FileCategory__c
                                          FROM ContentVersion
                                          WHERE IsLatest=true
                                          AND ContentDocumentId IN :uniqueFiles];
            for (ContentVersion file : files) file.SL_FileCategory__c = fileCategory;
            update files;
        }
        return 'SUCCESS';
    }

    @AuraEnabled
    public static WrappReturn getColumns(String fieldSet) {
        return fieldsDescribe(fieldSet);
    }

    @AuraEnabled
    public static String getRecordsForFilter(
        Id recId,
        String fieldName,
        Boolean communityVisible
    ) {
        if (recId == null) return null;
         List<ContentDocumentLink> documentLinks = null;
        if (communityVisible)
            documentLinks = [SELECT Id, ContentDocumentId, Visibility FROM ContentDocumentLink WHERE LinkedEntityId=:recId AND Visibility = 'AllUsers'];
        else
            documentLinks = [SELECT Id, ContentDocumentId, Visibility FROM ContentDocumentLink WHERE LinkedEntityId=:recId];

        if (documentLinks.size() > 0) {
            Set<Id> cdIds = setToList(documentLinks);
            String query = 'SELECT ' + String.escapeSingleQuotes(fieldName) + ' FROM ContentVersion WHERE IsLatest=true AND ContentDocumentId IN :cdIds ORDER BY ' + fieldName +' NULLS LAST';
            List<sObject> filterRecords = Database.query(query);
            return JSON.serialize(new List<Object>(filterRecordsByField(filterRecords, fieldName)));
        }
        return null;
    }

    private static Set<Object> filterRecordsByField(List<sObject> records, String fieldName) {
        Set<Object> recs =  new Set<Object>();
        for (sObject rec : records) {
            List<String> splittedFieldPath = fieldName.split('\\.');
            if (splittedFieldPath.size() > 1) {
                recs.add((Object)rec.getSobject(splittedFieldPath[0]).get(splittedFieldPath[1]));
                continue;
            }
            recs.add((Object)rec.get(fieldName));
        }
        return recs;
    }

    private static String createFilterString(List<ArgumentsWrapper> filters) {
        String filterString = '';
        for (ArgumentsWrapper filter : filters) {
            filterString += ' AND ' + filter.fieldName + '= \'' + escapeAndReplace(filter.value) + '\' ';
        }
        return filterString.replace('\'null\'','NULL');
    }

    private static String escapeAndReplace(String value) {
        if (value == 'nothingSelectedNULL') return null;
        return String.escapeSingleQuotes(value);
    }

    @AuraEnabled
    public static ResponseWrapper getFiles(
        String fieldSet,
        Id recId,
        String defaultSorting,
        String sortingColumn,
        Boolean communityVisible,
        String filterBy,
        String searchText
    ){
        List<ArgumentsWrapper> filters = null;
        if (String.isNotBlank(filterBy)) filters = (List<ArgumentsWrapper>)JSON.deserialize(filterBy, List<ArgumentsWrapper>.class);
        if (recId == null) return null;
        if (String.isEmpty(defaultSorting)) defaultSorting = ' DESC';
        if (String.isEmpty(sortingColumn)) sortingColumn = ' ORDER BY Title';
		if (!sortingColumn.containsIgnoreCase('ORDER BY')) sortingColumn = ' ORDER BY ' + sortingColumn;
        WrappReturn ret = fieldsDescribe(fieldSet);
        Set<String> setQueryString = ret.lstQueryStr;

        if (String.isNotEmpty(recId)) {
            List<ContentDocumentLink> documentLinks = null;
            if (communityVisible)
                documentLinks = [SELECT Id, ContentDocumentId, Visibility FROM ContentDocumentLink WHERE LinkedEntityId=:recId AND Visibility = 'AllUsers'];
            else
                documentLinks = [SELECT Id, ContentDocumentId, Visibility FROM ContentDocumentLink WHERE LinkedEntityId=:recId];

            if (documentLinks.size() > 0) {
                Set<Id> cdIds = setToList(documentLinks);
                Set<ID> searchDocIds = new Set<ID>();
                List<ContentVersion> lstrecords = new List<ContentVersion>();

                Set<String> lstObjFields = new Set<String>{'CreatedDate','LastModifiedDate', 'SL_FileCategory__c'};
				Set<String> lstObjFieldsFin = new Set<String>();
				lstObjFieldsFin.addAll(lstObjFields);
                lstObjFieldsFin.addAll(setQueryString);
                lstObjFieldsFin.add('ContentDocumentId');

                String sSql = 'SELECT ' + String.join(new List<String>(lstObjFieldsFin), ',');
                sSql += ' FROM ContentVersion ';
                sSql += 'WHERE IsLatest=true ';
                sSql += ' AND ContentDocumentId IN :cdIds ';
                if (filters != null && filters.size() > 0) sSql = sSql + createFilterString(filters);
                if (String.isNotBlank(searchText)) {
                    String soslQuery = 'FIND \'*' + String.escapeSingleQuotes(searchText) + '*\' IN ALL FIELDS RETURNING ContentVersion(ID, Title, VersionData WHERE IsLatest=true AND ContentDocumentId IN :cdIds)';
                    List<List<ContentVersion>> searchDocs = Search.query(soslQuery);
                    if(
                        searchDocs != null &&
                        searchDocs.size() > 0 &&
                        searchDocs[0].size() > 0
                    ) {
                        for(ContentVersion item:searchDocs[0]) searchDocIds.add(item.ID);
                    }
                    sSql += ' AND Id IN :searchDocIds ';
                }
                sSql += ' ' + sortingColumn;
                sSql += ' ' + defaultSorting;
                sSql += ' NULLS LAST';
                List<sObject> cVer = Database.query(sSql);
                return new ResponseWrapper(ret.lstFieldset, cVer, documentLinks);
            }
        }
        return null;
    }

    private static Set<Id> setToList(List<ContentDocumentLink> links) {
        Set<Id> ids = new Set<Id>();
        for (ContentDocumentLink cdl : links) ids.add(cdl.ContentDocumentId);
        return ids;
    }

    private static WrappReturn fieldsDescribe(String fieldSet) {
        Set<FieldsWrapper> lstFields = new Set<FieldsWrapper>();
    	Set<String> setFieldsToQuery = new set<String>();

    	Schema.DescribeSObjectResult obj = ContentVersion.sObjectType.getDescribe();

    	Schema.FieldSet fs1 = obj.fieldSets.getMap().get(fieldSet);

    	List<fieldSetMember> AllFields = fs1.getFields();

    	for(fieldSetMember field : AllFields){
    		String fieldApi = field.getFieldPath();

    		if(
				fieldApi != 'NetworkId' &&
				fieldApi != 'ContentDocumentId' &&
				fieldApi != 'ExternalDataSourceId' &&
				fieldApi !='ContentBodyId'
			){
	    		if(String.valueOf(field.getType()) == 'REFERENCE'){
	    			if(field.getFieldPath().toLowerCase().endsWith('__c')){
	    				setFieldsToQuery.add(field.getFieldPath().replace('__c','__r.Name'));
	    				lstFields.add(
                            new FieldsWrapper(
                                field.getFieldPath().replace('__c','__r.Name'),
                                field.getLabel(),
                                'REFERENCE'
                            )
                        );
	    			} else if (field.getFieldPath().toLowerCase().endsWith('id')){
						setFieldsToQuery.add(field.getFieldPath().replace('Id','.Name'));
						lstFields.add(
                            new FieldsWrapper(
                                field.getFieldPath().replace('Id','.Name'),
                                field.getLabel(),
                                'REFERENCE'
                            )
                        );
	    			}
	    		} else {
	    			setFieldsToQuery.add(field.getFieldPath());
	    			lstFields.add( new FieldsWrapper(
                            field.getFieldPath(),
                            field.getLabel(),
                            String.valueOf(field.getType())
                        )
                    );
	    		}
	    	}
    	}

    	WrappReturn objwrap = new WrappReturn(lstFields,setFieldsToQuery);
    	return objwrap;
    }



    public class ResponseWrapper {
        @AuraEnabled
        public Set<FieldsWrapper> fields {get;set;}

        @AuraEnabled
        public List<ContentVersion> files {get;set;}

        @AuraEnabled
        public List<ContentDocumentLink> links {get;set;}

        public ResponseWrapper(
            Set<FieldsWrapper> fields,
            List<ContentVersion> files,
            List<ContentDocumentLink> links
        ) {
            this.fields = fields;
            this.files = files;
            this.links = links;
        }
    }

    public class FieldsWrapper {
        @AuraEnabled
        public String fieldName { get;set; }
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String type { get; set; }

        public FieldsWrapper(
            String fieldPath,
            String strLabel,
            String strType
        ) {
            this.fieldName = fieldPath;
            this.label = strLabel;
            this.type = strType;
        }
    }
    public class WrappReturn{
    	@AuraEnabled
    	public Set<FieldsWrapper> lstFieldset {get;set;}
    	@AuraEnabled
    	public Set<String> lstQueryStr {get;set;}

        public wrappReturn(Set<FieldsWrapper> lstFieldset, Set<String> lstQueryStr){
        	this.lstQueryStr = lstQueryStr;
        	this.lstFieldset = lstFieldset;
        }
    }
}