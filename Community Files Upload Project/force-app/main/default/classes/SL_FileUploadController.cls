public class SL_FileUploadController {
    
    public class fileUploadParams {
        
        public String fileDescription;
        public String recordId;
        public List<String> lstOfDocumentIds;
        
    }
    
    public static void uploadDocument(String recordId, String fileDescription, List<String> lstOfDocumentIds){
        List<ContentVersion> lstContentDocuments = new List<ContentVersion>();
        
        for(String strDocId : lstOfDocumentIds){
            lstContentDocuments.add(new ContentVersion(ContentDocumentId = strDocId, 
                                                       Title = fileDescription,
                                                       FirstPublishLocationId = recordId
                                                      ));
        }
        
    }
}